const path = require('path');
const {override, addLessLoader} = require('customize-cra');

const overrideProcessEnv = value => config => {
  config.resolve.modules = [
    path.join(__dirname, 'src')
  ].concat(config.resolve.modules);
  config.resolve.alias = {
    halo: path.resolve(__dirname, 'src/helpers'),
    hai: path.resolve(__dirname, '/src/helpers'),
    oke: path.resolve(__dirname +  '/src/helpers')
  };
  return config;
};

module.exports = override(
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      '@primary-color': '#ffd500',
    }
  }),
  overrideProcessEnv({
    VERSION: JSON.stringify(require('./package.json').version),
  })
);
