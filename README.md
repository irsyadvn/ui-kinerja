# [E-Kinerja Kabupaten Minahasa Selatan](https:// "E-Kinerja")
E-Kinerja Daily Evaluation System. 
 
# Installation

**Note: E-Kinerja is using [npm](https://www.npmjs.com/get-npm)

Installing all the dependencies of project, run following command:

``` npm install ```

Run react server on local machine, run following command:

``` npm start ```
