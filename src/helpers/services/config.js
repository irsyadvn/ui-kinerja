// export const ENV = "PROD";
export const ENV = "DEV";
const localhost = "http://localhost:3001"; //david
const urlProduction = "https://jsonplaceholder.typicode.com";
export const baseURL = ENV === "PROD" ? urlProduction : localhost;
