import { baseURL } from '../config';
export const GET_SUB_DISTRICT_URL = baseURL + '/subDistrictList';
export const GET_EMPLOYEE_DATA_LIST = baseURL + '/employeeDataList';
