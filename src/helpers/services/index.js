// import { ENV } from 'config';
import { setRequestState } from "appRedux/actions/GenericActions";
import reducers from "appRedux/reducers/index";
import { createStore } from 'redux';
const store = createStore(reducers);

const isEmpty = (obj) => {
  for (let key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
};

class ApiService {
  open = (req) => {
    let {path, params, method} = req;
    if(isEmpty(params)) {
      params = {};
    }

    const setRequest = (inProgress, error) => {
      return setRequestState({
        [path]: {
          inProgress: inProgress,
          error: error
        }
      })
    };

    const dispatchAction = (action) => {
      try {
        store.dispatch(action);
      } catch (e) { }
    };

    const successCallback = (result, resolve) => {
      console.log('Success!', result);
      resolve(result);
      // wait for this thing implemented to redux
      dispatchAction(setRequest(false, false));
    };

    const errorCallback = (error, reject) => {
      console.log('Error!', error);
      try{
        alert(error);
      }catch(e){
        console.log(e)
      }
      reject(error);
      dispatchAction(setRequest(false, true));
    };

    dispatchAction(setRequest(true, false));

    return new Promise((resolve, reject) => {
      let data = !(isEmpty(params)) ? {
        ...params,
      } : {};
      if (!method) {
        //default to POST
        method = "POST";
      }
      console.log('Endpoint: ', path);
      fetch(
        path,
        {
          method: method === "POST" ? "POST" : "GET",
          headers : {
            "Content-Type": "application/json"
          },
          body: method === "POST" ? JSON.stringify(data) : undefined,
        }
      )
        .then((response) => response.json())
        .then(
          (result) => successCallback(result, resolve, reject),
          (error) => errorCallback(error, reject)
        );
    });
  }
}

export default new ApiService();
