import React, { Component } from "react";
import { IntlMessages } from "util/index";
import {Button, Card, Form, Breadcrumb, Table, Tooltip, Input} from "antd";
import ApiService from 'helpers/services';
import { GET_EMPLOYEE_DATA_LIST } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';
import AddDataPegawai from "./AddDataPegawai";

const Search = Input.Search;

class DataPegawai extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addAnnualSkpTarget',
      data: [],
      filteredData: [],
      loadingTable: true
    }
  }

  componentDidMount() {
    this.getEmployeeDataList();
  }

  getEmployeeDataList = () => {
    const req = {
        path : GET_EMPLOYEE_DATA_LIST,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          data: response.payload,
          filteredData: response.payload,
          loadingTable: false
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label
    });
  };

  saveTarget = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        console.log('Received values of form: ', fieldsValue);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deletePeriod = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  filterDataTable = (e) => {
    const { data } = this.state;
    const value = e.target.value;
    const filteredData = data.filter( x => x.name.toLowerCase().includes(value) || x.nip.includes(value));
    this.setState({
      filteredData: filteredData
    });
  };

  render() {
    let {sortedInfo, visible, modalLoading, modalLabel, deleteVisible, filteredData, loadingTable} = this.state;
    const { getFieldDecorator } = this.props.form;
    sortedInfo = sortedInfo || {};
    const columns = [
      {
        title: <IntlMessages id="employeeData.photo"/>,
        dataIndex: 'photo',
        key: 'photo',
        render: (text) => {
          return (
            <img src={require('assets/images/placeholder/'+text+'.png')} alt="profile"/>
          )
        },
        sorter: (a, b) => a.photo - b.photo,
        sortOrder: sortedInfo.columnKey === 'photo' && sortedInfo.order
      },
      {
        title: <IntlMessages id="employeeData.nip"/>,
        dataIndex: 'nip',
        key: 'nip'
      },
      {
        title: <IntlMessages id="profile.name"/>,
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: <IntlMessages id="profile.placeBornDate"/>,
        dataIndex: '',
        key: 'ttl',
        render: (text) => <span>{text.tempatLahir}, {text.tanggalLahir}</span>,
      },
      {
        title: <IntlMessages id="employeeData.gender"/>,
        dataIndex: 'gender',
        key: 'gender'
      },
      {
        title: <IntlMessages id="employeeData.phoneNumber"/>,
        dataIndex: 'phone',
        key: 'phone'
      },
      {
        title: <IntlMessages id="button.view"/>,
        dataIndex: '',
        key: 'view',
        render: (text) => <Tooltip placement="right" title={<IntlMessages id="button.view"/>}><Link to={'profile/'+text.nip} className="gx-link"><i className="icon fa fa-eye"/></Link></Tooltip>,
        width: 80
      },
      {
        title: <IntlMessages id="button.edit"/>,
        dataIndex: '',
        key: 'edit',
        render: (text) => <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}><Link to={'edit-profile/'+text.nip} className="gx-link"><i className="icon fa fa-pencil-alt"/></Link></Tooltip>,
        width: 80
      },
      {
        title: <IntlMessages id="button.delete"/>,
        dataIndex: '',
        key: 'hapus',
        render: () => <Tooltip placement="right" title={<IntlMessages id='button.delete'/>}><span className="gx-link" onClick={() => this.showModal('skp.deletePeriod')}><i className="icon fa fa-trash-alt"/></span></Tooltip>,
        width: 80
      }
    ];

    return (
      <div className="data-pegawai">
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="sidebar.employeeData"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='sidebar.employeeData'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.props.history.push('/tambah-pegawai')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.add"/></Button>
            <div>
              <label><IntlMessages id="button.search"/>: </label>
              <Search className='gx-mr-1' style={{width: 180}} onChange={this.filterDataTable}/>
            </div>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={filteredData} bordered={true} onChange={this.handleChange}
                 loading={loadingTable}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={<AddDataPegawai getFieldDecorator={getFieldDecorator}/>} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.saveTarget}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading} size='large' style={{top: 20}}/>
      </div>
    );
  }
}

const DataPegawaiForm = Form.create()(DataPegawai);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(DataPegawaiForm);
