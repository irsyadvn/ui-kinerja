import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import {Button, Card, Form, Breadcrumb, Table, Tooltip, DatePicker} from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';

const { MonthPicker } = DatePicker;

const data = [
  {
    key: '1',
    id: 1,
    year: 2019,
    month: 'JANUARI',
    skpScore: '44.83',
    approvalStatus: 'Disetujui'
  },
  {
    key: '2',
    id: 2,
    year: 2018,
    month: 'FEBRUARI',
    skpScore: '38.40',
    approvalStatus: 'Tidak Disetujui'
  },
];

class Bulanan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addPeriod'
    }
  }

  componentDidMount() {
    // this.prepareForService();
  }

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label
    });
  };

  savePeriod = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        const values = {
          ...fieldsValue,
          'month': fieldsValue['month'].format('YYYY-MM')
        };
        console.log('Received values of form: ', values);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deletePeriod = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  render() {
    let {sortedInfo, visible, modalLoading, modalLabel, deleteVisible} = this.state;
    const { getFieldDecorator } = this.props.form;
    sortedInfo = sortedInfo || {};
    const columns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => a.id - b.id,
        sortOrder: sortedInfo.columnKey === 'id' && sortedInfo.order,
        width: 30,
      },
      {
        title: <IntlMessages id='skp.year'/>,
        dataIndex: 'year',
        key: 'year',
        sorter: (a, b) => a.year - b.year,
        sortOrder: sortedInfo.columnKey === 'year' && sortedInfo.order,
      },
      {
        title: <IntlMessages id="skp.month"/>,
        dataIndex: 'month',
        key: 'month'
      },
      {
        title: <IntlMessages id="skp.skpScore"/>,
        dataIndex: 'skpScore',
        key: 'skpScore'
      },
      {
        title: <IntlMessages id="button.delete"/>,
        dataIndex: '',
        key: 'hapus',
        render: () => <Tooltip placement="right" title={<IntlMessages id='button.delete'/>}><span className="gx-link" onClick={() => this.showModal('skp.deletePeriod')}><i className="icon fa fa-trash-alt"/></span></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="skp.monthlySkpTarget"/>,
        dataIndex: '',
        key: 'targetSkp',
        render: () => <Tooltip placement="right" title={<IntlMessages id='skp.skpTarget'/>}><Link to='/skp-bulanan/target' className="gx-link"><i className="icon fa fa-file-alt"/></Link></Tooltip>
      },
      {
        title: <IntlMessages id="skp.monthlySkpRealization"/>,
        dataIndex: '',
        key: 'realisasi',
        render: () => <Tooltip placement="right" title={<IntlMessages id='skp.realization'/>}><Link to='/skp-bulanan/realisasi' className="gx-link"><i className="icon fa fa-tachometer-alt"/></Link></Tooltip>
      },
      {
        title: <IntlMessages id="skp.approvalStatus"/>,
        dataIndex: '',
        key: 'approvalStatus',
        render: (text) => text.approvalStatus === 'Disetujui' ? <div className="success-background approval-status">{text.approvalStatus}</div> : <div className="danger-background approval-status">{text.approvalStatus}</div>,
        width: 220
      }
    ];

    const formItemLayout = {
      labelCol: {xs: 24, sm: 6},
      wrapperCol: {xs: 24, sm: 18},
    };

    const savePeriodComponent =
      <Form>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.month'/>}>
          {getFieldDecorator('month', {
            rules: [{
              type: 'object',
              required: true,
              message: <ValidationMessages fieldName='skp.month' type='label.isRequired'/>
            }]
          })(<MonthPicker className="gx-mb-3 gx-w-100"/>)}
        </Form.Item>
      </Form>;

    return (
      <div>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="sidebar.monthlySkp"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='skp.monthlySkpList'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.showModal('skp.addPeriod')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.add"/></Button>
          </div>
          <Table className="gx-table-responsive center skp-bulanan-table" columns={columns} dataSource={data} bordered={true} onChange={this.handleChange}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={savePeriodComponent} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.savePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading}/>
      </div>
    );
  }
}

const BulananForm = Form.create()(Bulanan);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(BulananForm);
