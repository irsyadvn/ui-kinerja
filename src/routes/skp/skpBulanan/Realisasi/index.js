import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import { Button, Card, Form, Breadcrumb, Table, Tooltip, Input } from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';

const data = [];
for (let i = 1; i < 3; i++) {
  data.push({
    key: i,
    index: i,
    activity: 'Membangun e-kinerja 2.0',
    targetQuantity: '12 Dokumen',
    targetQuality: '100',
    targetTime: '12 bulan',
    targetCost: '0',
    realizationQuantity: '0 Dokumen',
    realizationQuality: '80.50',
    realizationTime: '12',
    realizationCost: '0',
    calculation: '157',
    score: '52.17'
  });
}

class Target extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addAnnualSkpTarget',
      dataSource: [],
      key: 0
    }
  }

  componentDidMount() {
    // this.prepareForService();
    this.setState({
      dataSource: data,
      key: data.length
    }, () => {
      this.totalScore();
    });
  }

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label
    });
  };

  saveTarget = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        console.log('Received values of form: ', fieldsValue);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deletePeriod = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  renderContent = (value, row, index) => {
    const obj = {
      children: value,
      props: {},
    };
    if (this.state.key <= index + 1) {
      obj.props.colSpan = 0;
    }
    return obj;
  };

  totalScore = () => {
    const { dataSource, key } = this.state;
    const newData = [
      {
        key: key + 1,
        index: <IntlMessages id='skp.skpScore'/>,
        score: '60.25'
      },
      {
        key: key + 2,
        index: <IntlMessages id='skp.additionalTask'/>,
        score: ''
      },
      {
        key: key + 3,
        index: <IntlMessages id='skp.additional1'/>,
        score: '1'
      },
      {
        key: key + 4,
        index: <IntlMessages id='skp.creativity'/>,
        score: ''
      },
      {
        key: key + 5,
        index: <IntlMessages id='skp.creativity1'/>,
        score: '6'
      },
      {
        key: key + 6,
        index: <IntlMessages id='skp.skpScoreTotal'/>,
        score: '67.25'
      },
    ];
    this.setState({
      dataSource: [...dataSource, ...newData],
      key: key + 1,
    });
  };

  render() {
    const { visible, modalLoading, modalLabel, deleteVisible, dataSource } = this.state;
    const { getFieldDecorator } = this.props.form;
    const columns = [
      {
        title: 'No',
        dataIndex: 'index',
        key: 'index',
        width: 30,
        render: (text, row, index) => {
          if (this.state.key > index + 1) {
            return <span>{text}</span>;
          }
          return {
            children: <span className="gx-link gx-text-left">{text}</span>,
            props: {
              colSpan: 11,
            },
          };
        },
      },
      {
        title: <IntlMessages id='skp.activity'/>,
        dataIndex: 'activity',
        key: 'activity',
        render: this.renderContent
      },
      {
        title: <IntlMessages id='skp.target'/>,
        children: [
          {
            title: <IntlMessages id='skp.quantity'/>,
            dataIndex: 'targetQuantity',
            key: 'targetQuantity',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.quality'/>,
            dataIndex: 'targetQuality',
            key: 'targetQuality',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.time'/>,
            dataIndex: 'targetTime',
            key: 'targetTime',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.cost'/>,
            dataIndex: 'targetCost',
            key: 'targetCost',
            render: this.renderContent
          }
        ],
      },
      {
        title: <IntlMessages id='skp.realization'/>,
        children: [
          {
            title: <IntlMessages id='skp.quantity'/>,
            dataIndex: 'realizationQuantity',
            key: 'realizationQuantity',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.quality'/>,
            dataIndex: 'realizationQuality',
            key: 'realizationQuality',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.time'/>,
            dataIndex: 'realizationTime',
            key: 'realizationTime',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.cost'/>,
            dataIndex: 'realizationCost',
            key: 'realizationCost',
            render: this.renderContent
          }
        ],
      },
      {
        title: <IntlMessages id='skp.judgment'/>,
        children: [
          {
            title: <IntlMessages id='skp.calculation'/>,
            dataIndex: 'calculation',
            key: 'calculation',
            render: this.renderContent
          },
          {
            title: <IntlMessages id='skp.score'/>,
            dataIndex: 'score',
            key: 'score'
          }
        ],
      },
      {
        title: <IntlMessages id='skp.inputCostAndTime'/>,
        dataIndex: '',
        key: 'inputCostAndTime',
        render: (value, row, index) => {
          if (this.state.key > index + 1) {
            return (
              <Tooltip placement="bottom" title={<IntlMessages id='skp.inputCostAndTime'/>}><span onClick={() => this.showModal('skp.inputCostAndTime')} className="gx-link"><i className="icon fa fa-file"/></span></Tooltip>
            )
          }
        },
        width: 120
      }
    ];

    const formItemLayout = {
      labelCol: {xs: 24, sm: 8},
      wrapperCol: {xs: 24, sm: 16},
    };

    const InputCostAndTimeComponent =
      <Form>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.costRealization'/>}>
          {getFieldDecorator('costRealization', {
            rules: [{
              message: <ValidationMessages fieldName='skp.costRealization' type='label.isRequired'/>
            }]
          })(<Input/>)}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.timeRealization'/>}>
          {getFieldDecorator('timeRealization', {
            rules: [{
              message: <ValidationMessages fieldName='skp.timeRealization' type='label.isRequired'/>
            }]
          })(<Input/>)}
        </Form.Item>
      </Form>;

    return (
      <div className='realisasi-component'>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><Link to="/skp-bulanan"><span className="gx-link"><IntlMessages id="sidebar.annualSkp"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="skp.realization"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='skp.annualSkpRealization'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-end">
            <Button><i className='fa fa-print gx-mr-1'/><IntlMessages id="button.print"/></Button>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={dataSource} bordered pagination={false}/>
          {/*<Table className="gx-table-responsive center" columns={columns} dataSource={data} bordered pagination={false}/>*/}
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={InputCostAndTimeComponent} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.saveTarget}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading}/>
      </div>
    );
  }
}

const TargetForm = Form.create()(Target);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(TargetForm);
