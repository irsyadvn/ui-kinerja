import React, { Component } from "react";
import { IntlMessages } from "util/index";
import { Button, Card, Form, Breadcrumb, Table, Tooltip } from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';
import AddTarget from "./AddTarget";

const data = [
  {
    key: '1',
    id: '1',
    year: 2019,
    monthlyActivity: 'Contoh turunan 1',
    hasInheritance: '',
    quantityTarget: '2 Data',
    qualityTarget: 100,
    timeTarget: '1 Hari',
    cost: '0',
    edit: 'N',
    delete: 'N',
    dropTarget: '',
    dropStatus: 'Y'
  },
  {
    key: '2',
    id: '2',
    year: 2019,
    monthlyActivity: 'Membangun e-kinerja 2.0',
    hasInheritance: 'Y',
    quantityTarget: '',
    qualityTarget: '',
    timeTarget: '',
    cost: '',
    edit: 'Y',
    delete: 'Y',
    dropTarget: '',
    dropStatus: ''
  },
  {
    key: '3',
    id: '2.a',
    year: 2019,
    monthlyActivity: 'Mengumpulkan data e-kinerja berdasarkan referensi instansi lain',
    inheritance: '',
    quantityTarget: '2 Data',
    qualityTarget: 100,
    timeTarget: '1 Hari',
    cost: '0',
    edit: 'Y',
    delete: 'Y',
    dropTarget: 'Y',
    dropStatus: ''
  }
];

class Target extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addAnnualSkpTarget'
    }
  }

  componentDidMount() {
    // this.prepareForService();
  }

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label
    });
  };

  saveTarget = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        console.log('Received values of form: ', fieldsValue);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deletePeriod = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  onChangeInheritance = (e) => {
    this.props.form.setFieldsValue({inheritance: e.target.value});
  };

  render() {
    let { visible, modalLoading, modalLabel, deleteVisible } = this.state;
    const { getFieldDecorator } = this.props.form;
    const columns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        width: 30,
      },
      {
        title: <IntlMessages id='skp.year'/>,
        dataIndex: 'year',
        key: 'year'
      },
      {
        title: <IntlMessages id="skp.monthlyActivity"/>,
        dataIndex: 'monthlyActivity',
        key: 'monthlyActivity'
      },
      {
        title: <IntlMessages id="skp.inheritance"/>,
        dataIndex: 'hasInheritance',
        key: 'hasInheritance',
        render: (text) =>
          text === 'Y' ?
          <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}>
            <span className="gx-link" onClick={() => this.showModal('skp.editAnnualSkpTarget')}><i className="icon fa fa-edit"/></span>
          </Tooltip> : null
      },
      {
        title: <IntlMessages id="skp.quantityTarget"/>,
        dataIndex: 'quantityTarget',
        key: 'quantityTarget'
      },
      {
        title: <IntlMessages id="skp.qualityTarget"/>,
        dataIndex: 'qualityTarget',
        key: 'qualityTarget'
      },
      {
        title: <IntlMessages id="skp.timeTarget"/>,
        dataIndex: 'timeTarget',
        key: 'timeTarget'
      },
      {
        title: <IntlMessages id="skp.cost"/>,
        dataIndex: 'cost',
        key: 'cost'
      },
      {
        title: <IntlMessages id="button.edit"/>,
        dataIndex: 'edit',
        key: 'edit',
        render: (text) =>
          text === 'Y' ?
          <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}>
            <span className="gx-link" onClick={() => this.showModal('skp.editAnnualSkpTarget')}><i className="icon fa fa-pencil-alt"/></span>
          </Tooltip> : null
      },
      {
        title: <IntlMessages id="button.delete"/>,
        dataIndex: 'delete',
        key: 'hapus',
        render: (text) =>
          text === 'Y' ?
          <Tooltip placement="right" title={<IntlMessages id='button.delete'/>}>
            <span className="gx-link" onClick={() => this.showModal('skp.deletePeriod')}><i className="icon fa fa-trash-alt"/></span>
          </Tooltip> : null
      },
      {
        title: <IntlMessages id="skp.dropTarget"/>,
        dataIndex: 'dropTarget',
        key: 'dropTarget',
        render: (text) =>
          text === 'Y' ?
            <Tooltip placement="right" title={<IntlMessages id='skp.dropTarget'/>}>
              <span className="gx-link"><i className="icon fa fa-box"/></span>
            </Tooltip> : null
      },
      {
        title: <IntlMessages id="skp.dropStatus"/>,
        dataIndex: 'dropStatus',
        key: 'dropStatus',
        render: (text) =>
          text === 'Y' ?
            <Tooltip placement="right" title={<IntlMessages id='skp.dropStatus'/>}>
              <span className="gx-link"><i className="icon fa fa-box-open"/></span>
            </Tooltip> : null
      }
    ];

    return (
      <div>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><Link to="/skp-bulanan"><span className="gx-link"><IntlMessages id="sidebar.monthlySkp"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="skp.target"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='skp.monthlySkpTarget'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.showModal('skp.addMonthlySkpTarget')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.add"/></Button>
            <Button><i className='fa fa-print gx-mr-1'/><IntlMessages id="button.print"/></Button>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={data} bordered={true} onChange={this.handleChange}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={<AddTarget getFieldDecorator={getFieldDecorator} onChangeInheritance={this.onChangeInheritance}/>} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.saveTarget}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading} style={{top: 20}}/>
      </div>
    );
  }
}

const TargetForm = Form.create()(Target);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(TargetForm);
