import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import { Form, Row, Col, Select, Input, Radio } from "antd";
import PropTypes from 'prop-types';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class AddTarget extends Component {
  constructor(state) {
    super(state);
    this.state = {
    }
  }

  render() {
    const { getFieldDecorator, onChangeInheritance } = this.props;
    const formItemLayout = {
      labelCol: {xs: 24, sm: 8},
      wrapperCol: {xs: 24, sm: 16},
    };

    const annualSkpList = [
      {id: '001', name: 'Memasukkan Data'}
    ];

    const quantityUnit = getFieldDecorator('quantityUnit', {
      initialValue: 'dokumen',
    })(
      <Select style={{width: 130}}>
        <Option value="dokumen">Dokumen</Option>
        <Option value="surat">Surat</Option>
        <Option value="prosesDraft">Proses Draft</Option>
      </Select>
    );

    return (
      <div className='add-target-component'>
        <Row>
          <Col lg={24} md={24} sm={24} xs={24}>
            <Form>
              <FormItem
                {...formItemLayout}
                label={<IntlMessages id='skp.month'/>} className='gx-mb-0'
              >
                <span className="ant-form-text">NOVEMBER</span>
              </FormItem>
              <FormItem
                {...formItemLayout}
                label={<IntlMessages id='skp.annualSkp'/>}>
                {getFieldDecorator('annualSkp', {
                  rules: [
                    {required: true, message: <ValidationMessages name='skp.annualSkp' type='label.isRequired'/>},
                  ],
                  initialValue: '001',
                })(
                  <Select placeholder={<IntlMessages id='skp.annualSkp'/>}>
                    {
                      annualSkpList.map((item, i) => (
                        <Option key={i} value={item.id}>{item.name}</Option>
                      ))
                    }
                  </Select>
                )}
              </FormItem>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.inheritance'/>}>
                {getFieldDecorator('inheritance', {
                  rules: [{
                    required: true,
                    message: <ValidationMessages fieldName='skp.inheritance' type='label.isRequired'/>
                  }],
                  initialValue: 1
                })(
                  <RadioGroup onChange={onChangeInheritance}>
                    <Radio value={1}><IntlMessages id="skp.yes"/></Radio>
                    <Radio value={2}><IntlMessages id="skp.no"/></Radio>
                  </RadioGroup>
                )}
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.quantityTarget'/>}>
                {getFieldDecorator('quantityTarget', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.quantityTarget' type='label.isRequired'/>
                  }]
                })(<Input addonAfter={quantityUnit} style={{width: '100%'}}/>)}
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.timeTarget'/>}>
                {getFieldDecorator('timeTarget', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.timeTarget' type='label.isRequired'/>
                  }]
                })(<div className='gx-d-flex'><Input className='gx-w-50' type={'number'} min={0}/>&nbsp;&nbsp;Hari</div>)}
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.monthlyCost'/>}>
                {getFieldDecorator('monthlyCost', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.monthlyCost' type='label.isRequired'/>
                  }]
                })(<div className='gx-d-flex'><Input className='gx-w-50'/>&nbsp;&nbsp;IDR</div>)}
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

AddTarget.propTypes = {
  getFieldDecorator: PropTypes.any,
  onChangeInheritance: PropTypes.any,
};

export default AddTarget;
