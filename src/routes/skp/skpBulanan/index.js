import React from "react";
import { Route, Switch } from "react-router-dom";

import asyncComponent from "util/asyncComponent";

const SkpBulanan = ({match}) => {
  return (
  <Switch>
    <Route exact path={`${match.url}`} component={asyncComponent(() => import('./Bulanan/'))}/>
    <Route path={`${match.url}/target`} component={asyncComponent(() => import('./Target/'))}/>
    <Route path={`${match.url}/realisasi`} component={asyncComponent(() => import('./Realisasi/'))}/>
  </Switch>
)};

export default SkpBulanan;
