import React, { Component } from "react";
import { IntlMessages, ValidationMessages, DateFormatter } from "util/index";
import {Button, Card, Form, Breadcrumb, Table, Tooltip, DatePicker} from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';
import { firebase } from '../../../../firebase/firebase';
import moment from 'moment';

const dateFormat = 'YYYY-MM-DD';

class Tahunan extends Component {
  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection('skp_tahunan');
    this.unsubscribe = null;
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addPeriod',
      skpTahunanList: [],
      loadingTable: true,
      selectedId: '',
      selectedIdx: '',
      biggestIdx: 0
    }
  }

  componentDidMount() {
    // this.prepareForService();
    this.unsubscribe = this.ref.orderBy('idx', 'asc').onSnapshot(this.getSkpTahunanList);
  }

  getSkpTahunanList = (querySnapshot) => {
    this.setState({loadingTable: true});
    const skpTahunanList = [];
    let biggestIdx = 0;
    querySnapshot.forEach((doc) => {
      const { idx, startDate, endDate } = doc.data();
      skpTahunanList.push({
        id: doc.id,
        doc, // DocumentSnapshot
        key: idx,
        startDate,
        endDate,
      });
      biggestIdx = idx >= biggestIdx ? idx : biggestIdx;
    });
    this.setState({
      skpTahunanList, loadingTable: false, biggestIdx
    });
    console.log('skp: ', this.state.skpTahunanList);
  };

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label, id, idx, startDate, endDate) => {
    if (id) {
      this.props.form.setFieldsValue({fromDate: moment(startDate, 'YYYY-MM-DD'), untilDate: moment(endDate, 'YYYY-MM-DD')});
    } else {
      this.props.form.resetFields();
    }
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label,
      selectedId: id ? id : '',
      selectedIdx: idx ? idx : '',
    });
  };

  savePeriod = (e) => {
    const { selectedId, selectedIdx, biggestIdx } = this.state;
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        if (selectedId === '') { // For Save new period
          this.ref.add({
            idx: biggestIdx + 1,
            startDate: fieldsValue['fromDate'].format('YYYY-MM-DD'),
            endDate: fieldsValue['untilDate'].format('YYYY-MM-DD')
          }).then((docRef) => {
            this.setState({modalLoading: false, visible: false});
            this.props.form.resetFields();
          })
            .catch((error) => {
              console.error("Error adding SKP: ", error);
            });
        } else { // For Edit period
          const startDate = this.props.form.getFieldValue('fromDate').format('YYYY-MM-DD');
          const endDate = this.props.form.getFieldValue('untilDate').format('YYYY-MM-DD');
          this.ref.doc(selectedId).set({
            idx: selectedIdx,
            startDate,
            endDate
          }).then((docRef) => {
            this.setState({modalLoading: false, visible: false, selectedId: ''});
            this.props.form.resetFields();
          })
            .catch((error) => {
              console.error("Error adding document: ", error);
            });
        }
      }
    });
  };

  deletePeriod = (e) => {
    const { selectedId } = this.state;
    e.preventDefault();
    this.setState({modalLoading: true});
    this.ref.doc(selectedId).delete().then(() => {
      console.log("Document successfully deleted!");
      this.setState({modalLoading: false, deleteVisible: false});
    }).catch((error) => {
      console.error("Error removing document: ", error);

    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  delete(id){
    firebase.firestore().collection('boards').doc(id).delete().then(() => {
      console.log("Document successfully deleted!");
      this.props.history.push("/")
    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
  }

  render() {
    let { sortedInfo, visible, modalLoading, modalLabel, deleteVisible, skpTahunanList, loadingTable } = this.state;
    const { getFieldDecorator } = this.props.form;
    sortedInfo = sortedInfo || {};
    const columns = [
      {
        title: 'No',
        dataIndex: 'key',
        key: 'key',
        sorter: (a, b) => a.key - b.key,
        sortOrder: sortedInfo.columnKey === 'key' && sortedInfo.order,
        width: 30,
      },
      {
        title: <IntlMessages id="skp.skpPeriod"/>,
        dataIndex: '',
        key: 'period',
        render: (text) => {
          return (
            <span><DateFormatter date={text.startDate} format='dd mon yyyy'/> - <DateFormatter date={text.endDate} format='dd mon yyyy'/></span>
          );
        }
      },
      {
        title: <IntlMessages id="button.edit"/>,
        dataIndex: '',
        key: 'edit',
        render: (text) => <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}><span className="gx-link" onClick={() => this.showModal('skp.editPeriod', text.id, text.key, text.startDate, text.endDate)}><i className="icon fa fa-pencil-alt"/></span></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="button.delete"/>,
        dataIndex: '',
        key: 'hapus',
        render: (text) => <Tooltip placement="right" title={<IntlMessages id='button.delete'/>}><span className="gx-link" onClick={() => this.showModal('skp.deletePeriod', text.id)}><i className="icon fa fa-trash-alt"/></span></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="skp.skpTarget"/>,
        dataIndex: '',
        key: 'targetSkp',
        render: () => <Tooltip placement="right" title={<IntlMessages id='skp.skpTarget'/>}><Link to='/skp-tahunan/target' className="gx-link"><i className="icon fa fa-file-alt"/></Link></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="skp.realization"/>,
        dataIndex: '',
        key: 'realisasi',
        render: () => <Tooltip placement="right" title={<IntlMessages id='skp.realization'/>}><Link to='/skp-tahunan/realisasi' className="gx-link"><i className="icon fa fa-tachometer-alt"/></Link></Tooltip>,
        width: 120
      }
    ];

    const formItemLayout = {
      labelCol: {xs: 24, sm: 12},
      wrapperCol: {xs: 24, sm: 12},
    };

    const savePeriodComponent =
      <Form>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.fromDate'/>}>
          {getFieldDecorator('fromDate', {
            rules: [{
              type: 'object',
              required: true,
              message: <ValidationMessages fieldName='skp.fromDate' type='label.isRequired'/>
            }]
          })(<DatePicker className="gx-mb-3 gx-w-100" format={dateFormat}/>)}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.untilDate'/>}>
          {getFieldDecorator('untilDate', {
            rules: [{
              type: 'object',
              required: true,
              message: <ValidationMessages fieldName='skp.untilDate' type='label.isRequired'/>
            }]
          })(<DatePicker className="gx-mb-3 gx-w-100" format={dateFormat}/>)}
        </Form.Item>
      </Form>;

    return (
      <div>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="sidebar.annualSkp"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='skp.annualSkpList'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.showModal('skp.addPeriod')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.add"/></Button>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={skpTahunanList} bordered={true} onChange={this.handleChange} loading={loadingTable}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={savePeriodComponent} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.savePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading}/>
      </div>
    );
  }
}

const TahunanForm = Form.create()(Tahunan);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(TahunanForm);
