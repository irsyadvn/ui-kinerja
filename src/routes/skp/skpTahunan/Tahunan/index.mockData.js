import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import {Button, Card, Form, Breadcrumb, Table, Tooltip, DatePicker} from "antd";
// import ApiService from 'helpers/services';
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';

const dateFormat = 'DD-MM-YYYY';

const data = [
  {
    key: '1',
    id: 1,
    period: '02 Jan 2018 - 30 Jun 2017',
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    id: 2,
    period: '06 Jul 2018 - 30 Ags 2017',
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    id: 3,
    period: '03 Sep 2018 - 30 Okt 2017',
    address: 'Sidney No. 1 Lake Park',
  },
  {
    key: '4',
    id: 4,
    period: '02 Nov 2018 - 30 Des 2017',
    address: 'London No. 2 Lake Park',
  },
];

class Tahunan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addPeriod'
    }
  }

  componentDidMount() {
    // this.prepareForService();
  }

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label
    });
  };

  savePeriod = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        const values = {
          ...fieldsValue,
          'fromDate': fieldsValue['fromDate'].format('YYYY-MM-DD'),
          'untilDate': fieldsValue['untilDate'].format('YYYY-MM-DD')
        };
        console.log('Received values of form: ', values);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deletePeriod = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  render() {
    let {sortedInfo, visible, modalLoading, modalLabel, deleteVisible} = this.state;
    const { getFieldDecorator } = this.props.form;
    sortedInfo = sortedInfo || {};
    const columns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => a.id - b.id,
        sortOrder: sortedInfo.columnKey === 'id' && sortedInfo.order,
        width: 30,
      },
      {
        title: <IntlMessages id="skp.skpPeriod"/>,
        dataIndex: 'period',
        key: 'period'
      },
      {
        title: <IntlMessages id="button.edit"/>,
        dataIndex: '',
        key: 'edit',
        render: () => <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}><span className="gx-link" onClick={() => this.showModal('skp.editPeriod')}><i className="icon fa fa-pencil-alt"/></span></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="button.delete"/>,
        dataIndex: '',
        key: 'hapus',
        render: () => <Tooltip placement="right" title={<IntlMessages id='button.delete'/>}><span className="gx-link" onClick={() => this.showModal('skp.deletePeriod')}><i className="icon fa fa-trash-alt"/></span></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="skp.skpTarget"/>,
        dataIndex: '',
        key: 'targetSkp',
        render: () => <Tooltip placement="right" title={<IntlMessages id='skp.skpTarget'/>}><Link to='/skp-tahunan/target' className="gx-link"><i className="icon fa fa-file-alt"/></Link></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="skp.realization"/>,
        dataIndex: '',
        key: 'realisasi',
        render: () => <Tooltip placement="right" title={<IntlMessages id='skp.realization'/>}><Link to='/skp-tahunan/realisasi' className="gx-link"><i className="icon fa fa-tachometer-alt"/></Link></Tooltip>,
        width: 120
      }
    ];

    const formItemLayout = {
      labelCol: {xs: 24, sm: 12},
      wrapperCol: {xs: 24, sm: 12},
    };

    const savePeriodComponent =
      <Form>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.fromDate'/>}>
          {getFieldDecorator('fromDate', {
            rules: [{
              type: 'object',
              required: true,
              message: <ValidationMessages fieldName='skp.fromDate' type='label.isRequired'/>
            }]
          })(<DatePicker className="gx-mb-3 gx-w-100" format={dateFormat}/>)}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.untilDate'/>}>
          {getFieldDecorator('untilDate', {
            rules: [{
              type: 'object',
              required: true,
              message: <ValidationMessages fieldName='skp.untilDate' type='label.isRequired'/>
            }]
          })(<DatePicker className="gx-mb-3 gx-w-100" format={dateFormat}/>)}
        </Form.Item>
      </Form>;

    return (
      <div>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="sidebar.annualSkp"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='skp.annualSkpList'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.showModal('skp.addPeriod')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.add"/></Button>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={data} bordered={true} onChange={this.handleChange}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={savePeriodComponent} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.savePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading}/>
      </div>
    );
  }
}

const TahunanForm = Form.create()(Tahunan);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(TahunanForm);
