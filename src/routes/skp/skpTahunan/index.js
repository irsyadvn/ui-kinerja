import React from "react";
import { Route, Switch } from "react-router-dom";

import asyncComponent from "util/asyncComponent";

const SkpTahunan = ({match}) => {
  return (
  <Switch>
    <Route exact path={`${match.url}`} component={asyncComponent(() => import('./Tahunan/'))}/>
    <Route path={`${match.url}/target`} component={asyncComponent(() => import('./Target/'))}/>
    <Route path={`${match.url}/realisasi`} component={asyncComponent(() => import('./Realisasi/'))}/>
  </Switch>
)};

export default SkpTahunan;
