import React from "react";
import {Route, Switch} from "react-router-dom";
import RequestList from "./request-list";
import UpdateKartuKeluarga from "./UpdateKartuKeluarga";


const JejakSipil = ({match}) => (
  <Switch>
    <Route path={`${match.url}/request-list`} component={RequestList}/>
    <Route path={`${match.url}/perubahan-kartu-keluarga`} component={UpdateKartuKeluarga}/>
  </Switch>
);

export default JejakSipil;
