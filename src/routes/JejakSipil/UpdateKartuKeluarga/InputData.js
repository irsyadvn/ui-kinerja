import React, {Component} from "react";
import ContainerHeader from "components/ContainerHeader/index";
// import IntlMessages from "util/IntlMessages";
import { Button, Card, Form, Icon, Input, Radio, Select, Upload, DatePicker } from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import {connect} from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class InputData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: []
    }
  }

  componentDidMount() {
    this.getTodoList();
  }

  getTodoList = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };
  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  render() {
    const {getFieldDecorator} = this.props.form;
    const formItemLayout = {
      labelCol: {xs: 24, sm: 6},
      wrapperCol: {xs: 24, sm: 14},
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '62',
    })(
      <Select style={{width: 70}}>
        <Option value="62">+62</Option>
      </Select>
    );
    const config = {
      rules: [{type: 'object', message: 'Pilih tanggal!'}],
    };
    const { subDistrictLoaded, subDistrictList } = this.state;
    return (
      <Card className="gx-card" title="Formulir Biodata Penduduk Untuk Perubahan Data Warga Negara Indonesia">
        {
          !subDistrictLoaded ? <h1>Loading...</h1> : <h1>{subDistrictList.title}</h1>
        }
        <Form onSubmit={this.handleSubmit}>
          <ContainerHeader title="Data Wilayah"/>
          <FormItem
            {...formItemLayout}
            label="Provinsi"
            hasFeedback
          >
            {getFieldDecorator('province', {
              rules: [
                {required: true, message: 'Pilih Provinsi!'},
              ],
            })(
              <Select placeholder="Pilih Provinsi">
                <Option value="sulawesi-utara">Sulawesi Utara</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Kabupaten/Kota"
            hasFeedback
          >
            {getFieldDecorator('kota', {
              rules: [
                {required: true, message: 'Pilih Kabupaten/Kota!'},
              ],
            })(
              <Select placeholder="Pilih Kabupaten/Kota">
                <Option value="minahasa-selatan">Minahasa Selatan</Option>
                <Option value="minahasa-selatan-utara">Minahasa Utara</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Kecamatan"
            hasFeedback
          >
            {getFieldDecorator('kecamatan', {
              rules: [
                {required: true, message: 'Pilih Kecamatan!'},
              ],
            })(
              <Select placeholder="Pilih Kecamatan">
                {
                  subDistrictLoaded ? subDistrictList.map((subDistrict, i) => {
                    return(<Option value={subDistrict.id} key={i}>{subDistrict.name}</Option>)
                  }) : null
                }
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Kelurahan/Desa"
            hasFeedback
          >
            {getFieldDecorator('kelurahan', {
              rules: [
                {required: true, message: 'Pilih Kelurahan/Desa!'},
              ],
            })(
              <Select placeholder="Pilih Kelurahan/Desa">
                <Option value="elusan">Elusan</Option>
                <Option value="pondos">Pondos</Option>
              </Select>
            )}
          </FormItem>

          <ContainerHeader title="Data Keluarga"/>
          <FormItem
            {...formItemLayout}
            label="Nama Kepala Keluarga">
            {getFieldDecorator('FatherName', {
              rules: [{required: true, message: 'Masukan Nama Kepala Keluarga!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="No. Kartu Keluarga">
            {getFieldDecorator('kkNumber', {
              rules: [{required: true, message: 'Masukan No. Kartu Keluarga!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Alamat Keluarga">
            {getFieldDecorator('address', {
              rules: [{required: true, message: 'Masukan Alamat Keluarga!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Telepon"
          >
            {getFieldDecorator('phone', {
              rules: [{required: true, message: 'Masukan Nomor Telepon!'}],
            })(
              <Input addonBefore={prefixSelector} style={{width: '100%'}}/>
            )}
          </FormItem>

          <ContainerHeader title="Data Individu"/>
          <FormItem
            {...formItemLayout}
            label="Nama Lengkap">
            {getFieldDecorator('fullName', {
              rules: [{required: true, message: 'Masukan Nama Lengkap!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="No. KTP">
            {getFieldDecorator('ktpNumber', {
              rules: [{required: true, message: 'Masukan No. KTP!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Alamat Sebelumnya">
            {getFieldDecorator('pastAddress', {
              rules: [{required: true, message: 'Masukan Alamat Sebelumnya!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="No. Paspor">
            {getFieldDecorator('passportNumber', {
              rules: [{required: false, message: 'Masukan No. Paspor!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Tanggal Berakhir Paspor"
          >
            {getFieldDecorator('endDatePassport', config)(
              <DatePicker className="gx-mb-3 gx-w-100"/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Jenis Kelamin"
          >
            {getFieldDecorator('gender')(
              <RadioGroup>
                <Radio value="male">Laki-laki</Radio>
                <Radio value="female">Perempuan</Radio>
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Tempat Lahir">
            {getFieldDecorator('bornPlace', {
              rules: [{required: false, message: 'Masukan Tempat Lahir!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Tanggal Lahir"
          >
            {getFieldDecorator('bornDate', config)(
              <DatePicker className="gx-mb-3 gx-w-100"/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Akta Kelahiran / Surat Kenal Lahir"
          >
            {getFieldDecorator('aktaLahir')(
              <RadioGroup>
                <Radio value="N">Tidak Ada</Radio>
                <Radio value="Y">Ada</Radio>
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Nomor Akte Kelahiran / Surat Kenal Lahir">
            {getFieldDecorator('nomorAkteLahir', {
              rules: [{required: false, message: 'Masukan Nomor Akte Kelahiran / Surat Kenal Lahir!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Golongan Darah"
            hasFeedback
          >
            {getFieldDecorator('bloodType', {
              rules: [
                {required: true, message: 'Pilih Golongan Darah!'},
              ],
            })(
              <Select placeholder="Golongan Darah">
                <Option value="a">A</Option>
                <Option value="b">B</Option>
                <Option value="ab">AB</Option>
                <Option value="o">O</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Agama / Kepercayaan"
            hasFeedback
          >
            {getFieldDecorator('religion', {
              rules: [
                {required: true, message: 'Pilih Agama / Kepercayaan!'},
              ],
            })(
              <Select placeholder="Agama / Kepercayaan">
                <Option value="1">Islam</Option>
                <Option value="2">Kristen</Option>
                <Option value="3">Katholik</Option>
                <Option value="4">Hindu</Option>
                <Option value="5">Budha</Option>
                <Option value="6">Khong Hucu</Option>
                <Option value="7">Lainnya</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Akta Perkawinan/Buku Nikah"
          >
            {getFieldDecorator('aktaKawin')(
              <RadioGroup>
                <Radio value="1">Tidak Ada</Radio>
                <Radio value="2">Ada</Radio>
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Nomor Akte Perkawinan / Buku Nikah">
            {getFieldDecorator('marriedNumber', {
              rules: [{required: false, message: 'Masukan Nomor Akte Perkawinan / Buku Nikah!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Tanggal Perkawinan"
          >
            {getFieldDecorator('marriedDate', config)(
              <DatePicker className="gx-mb-3 gx-w-100"/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Akta Perceraian/Surat Cerai"
          >
            {getFieldDecorator('aktaCerai')(
              <RadioGroup>
                <Radio value="1">Tidak Ada</Radio>
                <Radio value="2">Ada</Radio>
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Nomor Akta Perceraian/Surat Cerai">
            {getFieldDecorator('divorceNumber', {
              rules: [{required: false, message: 'Masukan Nomor Akta Perceraian/Surat Cerai!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Tanggal Perceraian"
          >
            {getFieldDecorator('divorceDate', config)(
              <DatePicker className="gx-mb-3 gx-w-100"/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Status Hubungan dlm Keluarga"
            hasFeedback
          >
            {getFieldDecorator('familyRelationship', {
              rules: [
                {required: true, message: 'Pilih Status Hubungan dlm Keluarga!'},
              ],
            })(
              <Select placeholder="Pilih Status Hubungan dlm Keluarga">
                <Option value="1">Kepala Keluarga</Option>
                <Option value="2">Suami</Option>
                <Option value="3">Isteri</Option>
                <Option value="4">Anak</Option>
                <Option value="5">Menantu</Option>
                <Option value="6">Cucu</Option>
                <Option value="7">Orang Tua</Option>
                <Option value="8">Mertua</Option>
                <Option value="9">Famili Lain</Option>
                <Option value="10">Pembantu</Option>
                <Option value="11">Lainnya</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Kelainan Fisik dan Mental"
          >
            {getFieldDecorator('abnormality\n')(
              <RadioGroup>
                <Radio value="N">Tidak Ada</Radio>
                <Radio value="Y">Ada</Radio>
              </RadioGroup>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Penyandang Cacat"
            hasFeedback
          >
            {getFieldDecorator('disabilities', {
              rules: [
                {required: false, message: 'Pilih Cacat!'},
              ],
            })(
              <Select>
                <Option value="1">Cacat Fisik</Option>
                <Option value="2">Cacat Netra/Buta</Option>
                <Option value="3">Cacat Rungu/Wicara</Option>
                <Option value="4">Cacat Mental/Jiwa</Option>
                <Option value="5">Cacat Fisik dan Mental</Option>
                <Option value="6">Cacat lainnya</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Pendidikan Terakhir"
            hasFeedback
          >
            {getFieldDecorator('education', {
              rules: [
                {required: true, message: 'Pilih Pendidikan Terakhir!'},
              ],
            })(
              <Select placeholder="Pilih Pendidikan Terakhir">
                <Option value="1">Tidak/Belum Sekolah</Option>
                <Option value="2">Tidak Tamat SD/Sederajat </Option>
                <Option value="3">Tamat SD/Sederajat</Option>
                <Option value="4">SLTP/Sederajat</Option>
                <Option value="5">SLTA/Sederjat</Option>
                <Option value="6">DiplomII</Option>
                <Option value="7">Akademi/Diploma III/S. Muda</Option>
                <Option value="8">DilpomaIV/Strata I</Option>
                <Option value="9">Strata II</Option>
                <Option value="10">Strata III</Option>
              </Select>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Jenis Pekerjaan">
            {getFieldDecorator('job', {
              rules: [{required: false, message: 'Masukan Jenis Pekerjaan!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <ContainerHeader title="Data Orang Tua"/>
          <FormItem
            {...formItemLayout}
            label="NIK Ibu">
            {getFieldDecorator('motherNIK', {
              rules: [{required: false, message: 'Masukan NIK Ibu!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Nama Lengkap Ibu">
            {getFieldDecorator('job', {
              rules: [{required: false, message: 'Masukan Nama Lengkap Ibu!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="NIK Ayah">
            {getFieldDecorator('motherNIK', {
              rules: [{required: false, message: 'Masukan NIK Ayah!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Nama Lengkap Ayah">
            {getFieldDecorator('job', {
              rules: [{required: false, message: 'Masukan Nama Lengkap Ayah!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <ContainerHeader title="Data Administrasi"/>

          <FormItem
            {...formItemLayout}
            label="Nama Ketua RT">
            {getFieldDecorator('job', {
              rules: [{required: false, message: 'Masukan Nama Ketua RT!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Nama Ketua RW">
            {getFieldDecorator('job', {
              rules: [{required: false, message: 'Masukan Nama Ketua RW!', whitespace: true}],
            })(
              <Input/>
            )}
          </FormItem>


          <ContainerHeader title="Lampiran"/>


          <FormItem
            {...formItemLayout}
            label="KK Sebelumnya"
          >
            {getFieldDecorator('pastKK', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload name="logo" action="/upload.do" listType="picture">
                <Button htmlType="button">
                  <Icon type="upload"/> Upload
                </Button>
              </Upload>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Foto Copy Akta Perkawinan"
          >
            {getFieldDecorator('uploadAktaKawin', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload name="logo" action="/upload.do" listType="picture">
                <Button htmlType="button">
                  <Icon type="upload"/> Upload
                </Button>
              </Upload>
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Foto Copy Akta Lahir"
          >
            {getFieldDecorator('uploadAktaLahir', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload name="logo" action="/upload.do" listType="picture">
                <Button htmlType="button">
                  <Icon type="upload"/> Upload
                </Button>
              </Upload>
            )}
          </FormItem>

          <FormItem
            wrapperCol={{xs: 24, sm: {span: 24, offset: 6}}} className="gx-text-right gx-mb-0">
            <Button type="primary" htmlType="submit">Submit</Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const InputDataForm = Form.create()(InputData);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, {
  setRequestState
})(InputDataForm);
