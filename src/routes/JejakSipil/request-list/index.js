import React from "react";
import {Card, Table} from "antd";
import {Col, Row} from "antd";
import {Link} from "react-router-dom";

const columns = [
  {
    title: 'List Permohonan Jejak Sipil',
    dataIndex: 'name',
    key: 'name',
    render: text => <Link to="/jejak-sipil/perubahan-kartu-keluarga" className="gx-link">{text}</Link>,
  },
];

const data = [
  {
    key: '1',
    name: 'Perubahan Data Kartu Keluarga Warga Negara Indonesia',
  },
  {
    key: '2',
    name: 'Permohonan Kartu Keluarga (KK) Baru Warga Negara Indonesia',
  }
];

const RequestList = () => {
  return (
    <Row>
      <Col span={24}>
        <Card>
          <Table className="gx-table-responsive" columns={columns} dataSource={data} pagination={false} showHeader={true}/>
        </Card>
      </Col>
    </Row>
  );
};

export default RequestList;
