import React from "react";

export const biodataList = [
  {
    id: 1,
    title: 'Status Kepegawaian',
    icon: 'company',
    userList: '',
    desc: ['PNS']
  },
  {
    id: 2,
    title: 'Tanggal Lahir',
    icon: 'birthday-new',
    userList: '',
    desc: ['Oct 25, 1984']
  },
  {
    id: 3,
    title: 'Pendidikan',
    icon: 'graduation',
    userList: '',
    desc: ['Telkom University']
  },
  {
    id: 4,
    title: 'Alamat',
    icon: 'home',
    userList: '',
    desc: ['Jalan S. Parman No.1. Slipi. Jakarta Barat. DKI Jakarta. 11530. Indonesia']
  },
  {
    id: 5,
    title: 'Jumlah Anggota Keluarga',
    icon: 'family',
    userList: '',
    desc: ['4']
  },
];

export const eventList = [
  {
    id: 1,
    image: 'https://via.placeholder.com/575x480',
    title: 'Sundance Film Festival.',
    address: 'Downsview Park, Toronto, Ontario',
    date: 'Feb 23, 2019',
  },
  {
    id: 2,
    image: 'https://via.placeholder.com/575x480',
    title: 'Underwater Musical Festival.',
    address: 'Street Sacramento, Toronto, Ontario',
    date: 'Feb 24, 2019',
  },
  {
    id: 3,
    image: 'https://via.placeholder.com/575x480',
    title: 'Village Feast Fac',
    address: 'Union Street Eureka',
    date: 'Oct 25, 2019',
  }
];

export const contactList = [
  {
    id: 1,
    title: 'Email',
    icon: 'email',
    desc: [<span className="gx-link" key={1}>riana.sukmawati@gmail.com</span>]
  },
  {
    id: 3,
    title: 'Phone',
    icon: 'phone',
    desc: ['081234567890']
  },
];

export const friendList = [
  {
    id: 1,
    image: 'https://via.placeholder.com/150x150',
    name: 'Chelsea Johns',
    status: 'online'

  },
  {
    id: 2,
    image: 'https://via.placeholder.com/150x150',
    name: 'Ken Ramirez',
    status: 'offline'
  },
  {
    id: 3,
    image: 'https://via.placeholder.com/150x150',
    name: 'Chelsea Johns',
    status: 'away'

  },
  {
    id: 4,
    image: 'https://via.placeholder.com/150x150',
    name: 'Ken Ramirez',
    status: 'away'
  },
];

export const scheduleList = [
  {
    id: 1,
    name: 'Pensiun',
    status: 'online'

  },
  {
    id: 2,
    name: 'Pangkat',
    status: 'offline'
  },
  {
    id: 3,
    name: 'Gaji',
    status: 'away'

  }
];

export const childrenList = [
  {
    key: '1',
    id: 1,
    nik: '3173052198266880',
    name: 'Ali Muchsin',
    ttl: 'Jakarta, 22 Maret 1990',
    education: 'S1',
    job: 'Karyawan Swasta',
    relation: 'Anak'
  },
  {
    key: '2',
    id: 2,
    nik: '3173052198266881',
    name: 'Agnes Monica',
    ttl: 'Jakarta, 22 Maret 1985',
    education: 'S1',
    job: 'Penyanyi',
    relation: 'Anak'
  },
  {
    key: '3',
    id: 3,
    nik: '3173052198266882',
    name: 'Abdul',
    ttl: 'Jakarta, 22 Maret 1989',
    education: 'SLTA',
    job: 'TKI',
    relation: 'Anak'
  }
];

export const parentList = [
  {
    key: '1',
    id: 1,
    nik: '3173393198266880',
    name: 'Imron',
    ttl: 'Jakarta, 22 Maret 1940',
    education: 'SLTA',
    job: 'Pengusaha',
    relation: 'Bapak'
  },
  {
    key: '2',
    id: 2,
    nik: '3173099198266881',
    name: 'Susi',
    ttl: 'Jakarta, 22 Maret 1945',
    education: 'SLTP',
    job: 'Ibu rumah tangga',
    relation: 'Ibu'
  },
];
