import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import { Button, Card, Form, Input } from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import SelectSearch from 'components/SelectSearch';

const FormItem = Form.Item;

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      changePassword: false
    }
  }

  componentDidMount() {
    // this.getTodoList();
  }

  getTodoList = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  showPasswordFields = () => {
    this.setState({
      changePassword: !this.state.changePassword
    });
  };

  render() {
    // const { nip } = this.props.match.params;
    const {getFieldDecorator} = this.props.form;
    const { changePassword } = this.state;
    const formItemLayout = {
      labelCol: {xs: 24, sm: 6},
      wrapperCol: {xs: 24, sm: 14},
    };

    const supervisorList = [
      {id: '001', name: 'User 1'},
      {id: '002', name: 'User 2'},
      {id: '003', name: 'User 3'},
    ];
    return (
      <Card className="gx-card" title={<IntlMessages id='sidebar.editProfile'/>}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            {...formItemLayout}
            label={<IntlMessages id='editProfile.nip'/>}>
            {getFieldDecorator('nip', {
              rules: [{
                required: false, whitespace: true,
                message: <ValidationMessages fieldName='editProfile.nip' type='label.isRequired'/>
              }]
            })(<Input disabled={true}/>)}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={<IntlMessages id='editProfile.name'/>}>
            {getFieldDecorator('name', {
              rules: [{
                required: false, whitespace: true,
                message: <ValidationMessages fieldName='editProfile.name' type='label.isRequired'/>
              }]
            })(<Input disabled={true}/>)}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={<IntlMessages id='editProfile.username'/>}>
            {getFieldDecorator('username', {
              rules: [{
                required: false, whitespace: true,
                message: <ValidationMessages fieldName='editProfile.username' type='label.isRequired'/>
              }]
            })(<Input disabled={true}/>)}
          </FormItem>

          <div className="ant-row ant-form-item">
            <div className='ant-form-item-label ant-col-xs-24 ant-col-sm-6'/>
            <div className='ant-form-item-label ant-col-xs-24 ant-col-sm-14 gx-text-left'>
              <Button className='gx-mb-0' onClick={this.showPasswordFields}>
                {
                  changePassword ? <IntlMessages id='button.cancel'/> : <IntlMessages id='editProfile.changePassword'/>
                }
              </Button>
            </div>
          </div>

          {
            changePassword ?
              <div>
                <FormItem
                  {...formItemLayout}
                  label={<IntlMessages id='editProfile.oldPassword'/>}>
                  {getFieldDecorator('oldPassword', {
                    rules: [{
                      required: true, whitespace: true,
                      message: <ValidationMessages fieldName='editProfile.oldPassword' type='label.isRequired'/>
                    }]
                  })(<Input type='password'/>)}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label={<IntlMessages id='editProfile.newPassword'/>}>
                  {getFieldDecorator('newPassword', {
                    rules: [{
                      required: true, whitespace: true,
                      message: <ValidationMessages fieldName='editProfile.newPassword' type='label.isRequired'/>
                    }]
                  })(<Input type='password'/>)}
                </FormItem>

                <FormItem
                  {...formItemLayout}
                  label={<IntlMessages id='editProfile.confirmNewPassword'/>}>
                  {getFieldDecorator('confirmNewPassword', {
                    rules: [{
                      required: true, whitespace: true,
                      message: <ValidationMessages fieldName='editProfile.confirmNewPassword' type='label.isRequired'/>
                    }]
                  })(<Input type='password'/>)}
                </FormItem>
              </div> : null
          }

          <FormItem
            {...formItemLayout}
            label={<IntlMessages id='editProfile.directSupervisor'/>}>
            <SelectSearch data={supervisorList} getFieldDecorator={getFieldDecorator} name='directSupervisor'
                          label='editProfile.directSupervisor' placeholder={<IntlMessages id='editProfile.searchByNameOrNip'/>}/>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={<IntlMessages id='editProfile.supervisor2'/>}>
            <SelectSearch data={supervisorList} getFieldDecorator={getFieldDecorator} name='supervisor2'
                          label='editProfile.supervisor2' placeholder={<IntlMessages id='editProfile.searchByNameOrNip'/>}/>
          </FormItem>

          <FormItem
            {...formItemLayout}
            label={<IntlMessages id='editProfile.supervisor3'/>}>
            <SelectSearch data={supervisorList} getFieldDecorator={getFieldDecorator} name='supervisor3'
                          label='editProfile.supervisor3' placeholder={<IntlMessages id='editProfile.searchByNameOrNip'/>}/>
          </FormItem>

          <FormItem
            wrapperCol={{xs: 24, sm: {span: 24, offset: 6}}} className="gx-text-right gx-mb-0">
            <Button type="primary" htmlType="submit"><IntlMessages id='button.save'/></Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const EditProfileForm = Form.create()(EditProfile);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, {
  setRequestState
})(EditProfileForm);
