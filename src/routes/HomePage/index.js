import React from "react";
import {Col, Row, Card} from "antd";
import Auxiliary from "util/Auxiliary";
import TinyBarChart from "components/chart/TinyBarChart";
import StackedAreaChart from "components/chart/StackedAreaChart";
import BalanceHistory from "components/chart/BalanceHistory";
import AnimatedGauge from "components/chart/AnimatedGauge";

const HomePage = () => {
  // const data = [
  //   {name: 'Jan', skp: 21, task: 11, productivity: 10},
  //   {name: 'Feb', skp: 12, task: 16, productivity: 7},
  //   {name: 'Mar', skp: 5, task: 8, productivity: 18},
  //   {name: 'Apr', skp: 11, task: 6, productivity: 13},
  //   {name: 'Mei', skp: 11, task: 20, productivity: 10},
  //   {name: 'Jun', skp: 4, task: 10, productivity: 15},
  //   {name: 'Jul', skp: 9, task: 10, productivity: 19},
  //   {name: 'Ags', skp: 17, task: 17, productivity: 19},
  //   {name: 'Sep', skp: 22, task: 6, productivity: 9},
  //   {name: 'Okt', skp: 20, task: 13, productivity: 16},
  //   {name: 'Nov', skp: 7, task: 10, productivity: 6},
  //   {name: 'Des', skp: 15, task: 20, productivity: 10}
  // ];

  const data = [
    {name: '', balance: 200},
    {name: 'JAN', balance: 400},
    {name: 'FEB', balance: 150},
    {name: 'MAR', balance: 400},
    {name: 'APR', balance: 1000},
    {name: 'MAY', balance: 400},
    {name: 'JUN', balance: 1200},
    {name: 'JUL', balance: 1000},
    {name: 'AUG', balance: 800},
    {name: 'SEP', balance: 750},
    {name: 'OCT', balance: 1500},
    {name: 'NOV', balance: 1000},
    {name: 'DEC', balance: 1500},
    {name: '', balance: 500},
  ];
  const bars = [
    {dataKey: 'skp', name: 'SKP', fill: 'rgba(28, 149, 243, 0.5)'},
    {dataKey: 'task', name: 'Tugas Tambahan', fill: 'rgba(255,69,0, 0.5)'},
    {dataKey: 'productivity', name: 'Produktivitas', fill: 'rgba(255,213,0, 0.5)'}
  ];
  return (
    <Auxiliary>
      <Row>
        <Col lg={8} md={8} sm={24} xs={24}>
            <BalanceHistory data={data} areas={bars} title='SKP' />
        </Col>
        <Col lg={8} md={8} sm={24} xs={24}>
          <BalanceHistory data={data} areas={bars} title='Tugas Tambahan' />
        </Col>
        <Col lg={8} md={8} sm={24} xs={24}>
          <BalanceHistory data={data} areas={bars} title='Produktivitas' />
        </Col>
      </Row>
      <Row className='gx-justify-content-center'>
        <Col lg={12} md={12} sm={24} xs={24}>
          <Card className="gx-card" bodyStyle={{paddingBottom: 0, paddingTop: 48}} title="RATA-RATA KUALITAS TAHUN 2019">
            <AnimatedGauge/>
          </Card>
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default HomePage;
