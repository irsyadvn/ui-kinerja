import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import { Button, Card, Form, Breadcrumb, Table, Tooltip, DatePicker, Select, Input } from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import { Link } from "react-router-dom";
import ModalComponent from 'components/ModalComponent';

const dateFormat = 'DD-MM-YYYY';
const Option = Select.Option;
const { TextArea } = Input;

const data = [
  // {
  //   key: '1',
  //   id: 1,
  //   date: '11 Nov 2019',
  //   creativity: 'Kreatifitas 2',
  //   quantity: '1 Dokumen'
  // }
];

class Kreatifitas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'creativity.inputCreativity'
    }
  }

  componentDidMount() {
    // this.prepareForService();
  }

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'creativity.deleteCreativity',
      deleteVisible: label === 'creativity.deleteCreativity',
      modalLabel: label
    });
  };

  savePeriod = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        const values = {
          ...fieldsValue,
          'date': fieldsValue['date'].format('YYYY-MM-DD')
        };
        console.log('Received values of form: ', values);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deleteCreativity = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  render() {
    let {sortedInfo, visible, modalLoading, modalLabel, deleteVisible} = this.state;
    const { getFieldDecorator } = this.props.form;
    sortedInfo = sortedInfo || {};
    const columns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => a.id - b.id,
        sortOrder: sortedInfo.columnKey === 'id' && sortedInfo.order,
        width: 30,
      },
      {
        title: <IntlMessages id='label.date'/>,
        dataIndex: 'date',
        key: 'date',
        sorter: (a, b) => a.date - b.date,
        sortOrder: sortedInfo.columnKey === 'date' && sortedInfo.order
      },
      {
        title: <IntlMessages id="sidebar.creativity"/>,
        dataIndex: 'creativity',
        key: 'creativity'
      },
      {
        title: <IntlMessages id="skp.quantity"/>,
        dataIndex: 'quantity',
        key: 'quantity'
      },
      {
        title: <IntlMessages id="button.edit"/>,
        dataIndex: '',
        key: 'edit',
        render: () => <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}><span className="gx-link" onClick={() => this.showModal('creativity.editCreativity')}><i className="icon fa fa-pencil-alt"/></span></Tooltip>,
        width: 120
      },
      {
        title: <IntlMessages id="button.delete"/>,
        dataIndex: '',
        key: 'hapus',
        render: () => <Tooltip placement="right" title={<IntlMessages id='button.delete'/>}><span className="gx-link" onClick={() => this.showModal('creativity.deleteCreativity')}><i className="icon fa fa-trash-alt"/></span></Tooltip>,
        width: 120
      }
    ];

    const formItemLayout = {
      labelCol: {xs: 24, sm: 6},
      wrapperCol: {xs: 24, sm: 18},
    };

    const quantityUnit = getFieldDecorator('quantityUnit', {
      initialValue: 'dokumen',
    })(
      <Select style={{width: 130}}>
        <Option value="dokumen">Dokumen</Option>
        <Option value="surat">Surat</Option>
        <Option value="prosesDraft">Proses Draft</Option>
      </Select>
    );

    const saveCreativityComponent =
      <Form>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='label.date'/>}>
          {getFieldDecorator('date', {
            rules: [{
              type: 'object',
              required: true,
              message: <ValidationMessages fieldName='label.date' type='label.isRequired'/>
            }]
          })(<DatePicker className="gx-mb-3 gx-w-100" format={dateFormat}/>)}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='sidebar.creativity'/>}>
          {getFieldDecorator('creativity', {
            rules: [{
              required: true, whitespace: true,
              message: <ValidationMessages fieldName='sidebar.creativity' type='label.isRequired'/>
            }]
          })(<TextArea rows={4}/>)}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label={<IntlMessages id='skp.quantity'/>}>
          {getFieldDecorator('quantity', {
            rules: [{
              required: true, whitespace: true,
              message: <ValidationMessages fieldName='skp.quantity' type='label.isRequired'/>
            }]
          })(<Input addonAfter={quantityUnit} style={{width: '100%'}}/>)}
        </Form.Item>
      </Form>;

    return (
      <div>
        <Card className="gx-card">
          <Breadcrumb>
            <Breadcrumb.Item><Link to="/home"><span className="gx-link"><IntlMessages id="sidebar.home"/></span></Link></Breadcrumb.Item>
            <Breadcrumb.Item><IntlMessages id="sidebar.creativity"/></Breadcrumb.Item>
          </Breadcrumb>
        </Card>

        <Card title={<IntlMessages id='creativity.creativityList'/>}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.showModal('creativity.inputCreativity')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.input"/></Button>
            <div>
              <label><IntlMessages id="skp.period"/>: </label>
              <Select className='gx-mr-1' style={{width: 130}} placeholder={<IntlMessages id='label.selectMonth'/>}>
                <Option value="01">Januari</Option>
                <Option value="02">Februari</Option>
                <Option value="03">Maret</Option>
                <Option value="04">April</Option>
                <Option value="05">Mei</Option>
                <Option value="06">Juni</Option>
                <Option value="07">Juli</Option>
                <Option value="08">Agustus</Option>
                <Option value="09">September</Option>
                <Option value="10">Oktober</Option>
                <Option value="11">November</Option>
                <Option value="12">Desember</Option>
              </Select>
              <Select className='gx-mr-1' style={{width: 130}} placeholder={<IntlMessages id='label.selectYear'/>}>
                <Option value="2019">2019</Option>
                <Option value="2018">2018</Option>
                <Option value="2017">2017</Option>
                <Option value="2016">2016</Option>
                <Option value="2015">2015</Option>
              </Select>
              <Button className='gx-mb-0' onClick={() => this.showModal('creativity.inputCreativity')}><i className='fa fa-search gx-mr-1'/><IntlMessages id="button.search"/></Button>
            </div>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={data} bordered={true} onChange={this.handleChange}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deleteCreativity}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={saveCreativityComponent} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.savePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading}/>
      </div>
    );
  }
}

const KreatifitasForm = Form.create()(Kreatifitas);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(KreatifitasForm);
