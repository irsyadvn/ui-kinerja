import React from "react";
import {Route, Switch} from "react-router-dom";
// import asyncComponent from "util/asyncComponent";
import HomePage from "./HomePage";
import JejakSipil from "./JejakSipil";
import EditProfile from "./EditProfile";
import SkpTahunan from "./skp/skpTahunan";
import SkpBulanan from "./skp/skpBulanan";
import Kreatifitas from "./Kreatifitas";
import Profile from "./Profile";
import DataPegawai from "./DataPegawai";

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}home`} component={HomePage}/>
      <Route path={`${match.url}jejak-sipil`} component={JejakSipil}/>
      <Route path={`${match.url}edit-profile/:nip`} component={EditProfile}/>
      <Route path={`${match.url}tambah-pegawai`} component={EditProfile}/>
      <Route path={`${match.url}skp-tahunan`} component={SkpTahunan}/>
      <Route path={`${match.url}skp-bulanan`} component={SkpBulanan}/>
      <Route path={`${match.url}kreatifitas`} component={Kreatifitas}/>
      <Route path={`${match.url}profile/:nip?`} component={Profile}/>
      <Route path={`${match.url}data-pegawai`} component={DataPegawai}/>
    </Switch>
  </div>
);

export default App;
