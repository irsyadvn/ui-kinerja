import React from "react";
import {Bar, BarChart, CartesianGrid, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";

const TinyBarChart = ({data, bars}) => (

  <ResponsiveContainer width="100%" height={350}>
    <BarChart data={data}
              margin={{top: 10, right: 0, left: -15, bottom: 0}}>
      <XAxis dataKey="name"/>
      <YAxis/>
      <CartesianGrid strokeDasharray="3 3"/>
      <Tooltip/>
      <Legend/>
      {
        bars.map((bar, i) => {
          return (
            <Bar key={i} dataKey={bar.dataKey} name={bar.name} fill={bar.fill}/>
          )
        })
      }
    </BarChart>
  </ResponsiveContainer>
);

export default TinyBarChart;
