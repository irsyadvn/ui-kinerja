import React from "react";
import {Area, AreaChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis} from "recharts";

const StackedAreaChart = ({data, areas}) => (
  <ResponsiveContainer width="100%" height={350}>
    <AreaChart data={data}
               margin={{top: 10, right: 0, left: -15, bottom: 0}}>
      <XAxis dataKey="name"/>
      <YAxis/>
      <CartesianGrid strokeDasharray="3 3"/>
      <Tooltip/>
      {
        areas.map((area, i) => {
          return (
            <Area type='monotone' stackId="1" fillOpacity={1} key={i} dataKey={area.dataKey} name={area.name} stroke={area.fill} fill={area.fill}/>
          )
        })
      }
    </AreaChart>
  </ResponsiveContainer>
);

export default StackedAreaChart;
