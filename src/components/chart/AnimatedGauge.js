import React, {Component} from "react";
import AmCharts from "@amcharts/amcharts3-react";

class AnimatedGauge extends Component {


  state = {
    randomValue: 60
  };

  componentDidMount() {
    this.interval = setInterval(() => this.randomValue(), 2000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  randomValue() {
    const min = 1;
    const max = 100;
    const randomValue = min + Math.random() * (max - min);
    this.setState({randomValue});
  }

  render() {
    const config = {
      "theme": "light",
      "type": "gauge",
      "axes": [{
        // inner circle
        "id": "axis1",
        "labelsEnabled": false,
        "axisColor": "#808080",
        "axisAlpha": 1,
        "tickAlpha": 0,
        "radius": "10%",
        "startAngle": 0,
        "endAngle": 360,
        "topTextFontSize": 20,
        "topTextYOffset": -70,
        "topText": "Kurang",
        "bottomText": 57.65,
        "bottomTextYOffset": -13,
      }, {
        // red ticks
        "id": "axis2",
        "startAngle": -90,
        "endAngle": 0,
        "endValue": 49,
        "radius": "100%",
        "axisAlpha": 0,
        "axisThickness": 0,
        "valueInterval": 1,
        "minorTickInterval": 4,
        "tickColor": "#ff0000",
        "tickLength": 40,
        "labelsEnabled": true,
        "labelFrequency": 10,
        // text inside center circle
        "topTextFontSize": 20,
        "topTextYOffset": 130,
        "topTextColor": "#808080",
      },
        {
          // orange ticks
          "id": "axis2",
          "startAngle": 0,
          "endAngle": 18,
          "startValue": 50,
          "endValue": 59,
          "radius": "100%",
          "axisAlpha": 0,
          "axisThickness": 0,
          "valueInterval": 1,
          "minorTickInterval": 4,
          "tickColor": "#FF8042",
          "tickLength": 40,
          "labelsEnabled": true,
          "labelFrequency": 10,
          // text inside center circle
          "topTextFontSize": 20,
          "topTextYOffset": 130,
          "topTextColor": "#808080",
        },
        {
          // yellow ticks
          "id": "axis2",
          "startAngle": 18,
          "endAngle": 45,
          "startValue": 60,
          "endValue": 74,
          "radius": "100%",
          "axisAlpha": 0,
          "axisThickness": 0,
          "valueInterval": 1,
          "minorTickInterval": 4,
          "tickColor": "rgb(236, 219, 35)",
          "tickLength": 40,
          "labelsEnabled": true,
          "labelFrequency": 10,
          // text inside center circle
          "topTextFontSize": 20,
          "topTextYOffset": 130,
          "topTextColor": "#808080",
        },
        {
          // light green ticks
          "id": "axis2",
          "startAngle": 45,
          "endAngle": 54,
          "startValue": 75,
          "endValue": 79,
          "radius": "100%",
          "axisAlpha": 0,
          "axisThickness": 0,
          "valueInterval": 1,
          "minorTickInterval": 4,
          "tickColor": "rgb(174, 226, 40)",
          "tickLength": 40,
          "labelsEnabled": false,
          "labelFrequency": 5,
          // text inside center circle
          "topTextFontSize": 20,
          "topTextYOffset": 130,
          "topTextColor": "#808080",
        },
        {
          // light green ticks
          "id": "axis2",
          "startAngle": 54,
          "endAngle": 72,
          "startValue": 80,
          "endValue": 89,
          "radius": "100%",
          "axisAlpha": 0,
          "axisThickness": 0,
          "valueInterval": 1,
          "minorTickInterval": 4,
          "tickColor": "rgb(174, 226, 40)",
          "tickLength": 40,
          "labelsEnabled": true,
          "labelFrequency": 10,
          // text inside center circle
          "topTextFontSize": 20,
          "topTextYOffset": 130,
          "topTextColor": "#808080",
        },
        {
        // dark green ticks
        "id": "axis2",
        "startAngle": 72,
        "endAngle": 90,
        "startValue": 90,
        "endValue": 100,
        "radius": "100%",
        "valueInterval": 1,
        "minorTickInterval": 4,
        "axisAlpha": 0,
        "axisThickness": 0,
        "tickColor": "rgb(106, 215, 45)",
        "tickLength": 40,
        "labelsEnabled": true,
        "labelFrequency": 10,
      }],
      "arrows": [{
        "axis": "axis2",
        "color": "#808080",
        "innerRadius": "10%",
        "nailRadius": 10,
        "radius": "70%",
        "value": 57.65
      }],
    };
    return (
      <div className="App gx-position-relative">
        <AmCharts.React style={{width: "100%", height: "350px"}} options={config}/>
      </div>
    )
  }
}

export default AnimatedGauge;
