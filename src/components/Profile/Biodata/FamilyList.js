import React, { Component } from "react";
import { IntlMessages } from "util/index";
import { Button, Card, Form, Table, Tooltip } from "antd";
import ApiService from 'helpers/services';
import { GET_SUB_DISTRICT_URL } from 'helpers/services/endpoints';
import { connect } from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import ModalComponent from 'components/ModalComponent';
import FamilyDetail from "./FamilyDetail";

class FamilyList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      filteredInfo: null,
      sortedInfo: null,
      modalLoading: false,
      visible: false,
      deleteVisible: false,
      modalLabel: 'skp.addAnnualSkpTarget'
    }
  }

  componentDidMount() {
    // this.prepareForService();
  }

  prepareForService = () => {
    const req = {
        path : GET_SUB_DISTRICT_URL,
        method: 'GET'
      },
      reqSuccess = (response) => {
        this.setState({
          subDistrictLoaded: true,
          subDistrictList: response.payload
        });
      },
      reqError = (error) => {
      };
    ApiService.open(req).then(reqSuccess, reqError);
  };

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  showModal = (label) => {
    this.setState({
      visible: label !== 'skp.deletePeriod',
      deleteVisible: label === 'skp.deletePeriod',
      modalLabel: label
    });
  };

  saveTarget = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        this.setState({modalLoading: true});
        console.log('Received values of form: ', fieldsValue);

        // Fake Loading
        setTimeout(() => {
          this.setState({modalLoading: false, visible: false});
        }, 1500);
      }
    });
  };

  deletePeriod = (e) => {
    e.preventDefault();
    this.setState({modalLoading: true});
    // Fake Loading
    setTimeout(() => {
      this.setState({modalLoading: false, deleteVisible: false});
    }, 1500);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      deleteVisible: false,
    });
  };

  render() {
    const { data, modalFormLayout, relationState } = this.props;
    let {sortedInfo, visible, modalLoading, modalLabel, deleteVisible} = this.state;
    const { getFieldDecorator } = this.props.form;
    sortedInfo = sortedInfo || {};
    const columns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => a.id - b.id,
        sortOrder: sortedInfo.columnKey === 'id' && sortedInfo.order,
        width: 20,
      },
      {
        title: <IntlMessages id="profile.nik"/>,
        dataIndex: 'nik',
        key: 'nik'
      },
      {
        title: <IntlMessages id="profile.name"/>,
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: <IntlMessages id="profile.ttl"/>,
        dataIndex: 'ttl',
        key: 'ttl'
      },
      {
        title: <IntlMessages id="profile.education"/>,
        dataIndex: 'education',
        key: 'education'
      },
      {
        title: <IntlMessages id="profile.job"/>,
        dataIndex: 'job',
        key: 'job'
      },
      {
        title: <IntlMessages id="profile.relation"/>,
        dataIndex: 'relation',
        key: 'relation'
      },
      {
        title: <IntlMessages id="button.edit"/>,
        dataIndex: '',
        key: 'edit',
        render: () => <Tooltip placement="right" title={<IntlMessages id="button.edit"/>}><span className="gx-link" onClick={() => this.showModal('button.edit')}><i className="icon fa fa-pencil-alt"/></span></Tooltip>,
      }
    ];

    return (
      <div>
        <Card bordered={false} bodyStyle={{paddingLeft: 16, paddingRight: 16, paddingBottom: 0}}>
          <div className="table-operations gx-d-flex ant-row-flex-space-between">
            <Button onClick={() => this.showModal('skp.addAnnualSkpTarget')}><i className='fa fa-plus gx-mr-1'/><IntlMessages id="button.add"/></Button>
          </div>
          <Table className="gx-table-responsive center" columns={columns} dataSource={data} bordered={true} onChange={this.handleChange} pagination={false}/>
        </Card>

        <ModalComponent type='delete' visible={deleteVisible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.deletePeriod}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.delete'/>} loading={modalLoading}/>

        <ModalComponent children={<FamilyDetail getFieldDecorator={getFieldDecorator} formItemLayout={modalFormLayout} isEditState={true} relationState={relationState} />} visible={visible} title={<IntlMessages id={modalLabel}/>} onSubmit={this.saveTarget}
                        onCancel={this.handleCancel} submitTextButton={<IntlMessages id='button.save'/>} loading={modalLoading} style={{top: 20}}/>
      </div>
    );
  }
}

const FamilyListForm = Form.create()(FamilyList);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, { setRequestState })(FamilyListForm);
