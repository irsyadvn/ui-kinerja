import React from "react";
import { Col, Form, Row, Tabs } from "antd";
import Widget from "components/Widget";
import { biodataList, childrenList, parentList } from '../../../routes/Profile/data';
import BiodataItem from "./BiodataItem";
// import { IntlMessages, ValidationMessages } from "util/index";
import {connect} from "react-redux";
import { setRequestState } from "appRedux/actions/GenericActions";
import FamilyDetail from './FamilyDetail';
import FamilyList from './FamilyList';

const TabPane = Tabs.TabPane;

const suamiIstriFormLayout = {
  labelCol: {xs: 9, sm: 7},
  wrapperCol: {xs: 15, sm: 15},
};

const modalFormLayout = {
  labelCol: {xs: 9, sm: 9},
  wrapperCol: {xs: 15, sm: 15},
};

class Biodata extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      subDistrictLoaded: false,
      subDistrictList: [],
      changePassword: false
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;

    return (
      <Widget title="Biodata" styleName="gx-card-tabs gx-card-tabs-right gx-card-profile">
        <Tabs defaultActiveKey="1">
          <TabPane tab="Profil" key="1">
            <div className="gx-mb-2">
              <Row>
                {biodataList.map((biodata, index) =>
                  <Col key={index} xl={8} lg={12} md={12} sm={12} xs={24}>
                    <BiodataItem data={biodata}/>
                  </Col>
                )}
              </Row>
            </div>
          </TabPane>
          <TabPane tab="Suami / Istri" key="2">
            <div className="gx-mb-2">
              <FamilyDetail getFieldDecorator={getFieldDecorator} formItemLayout={suamiIstriFormLayout} relationState='suamiIstri'/>
            </div>
          </TabPane>
          <TabPane tab="Anak" key="3">
            <div className="gx-mb-2">
              <Row>
                <FamilyList data={childrenList} getFieldDecorator={getFieldDecorator} modalFormLayout = {modalFormLayout} relationState='anak' />
              </Row>
            </div>
          </TabPane>
          <TabPane tab="Orang Tua" key="4">
            <div className="gx-mb-2">
              <Row>
                <FamilyList data={parentList} getFieldDecorator={getFieldDecorator} modalFormLayout = {modalFormLayout} relationState='orangTua' />
              </Row>
            </div>
          </TabPane>
        </Tabs>
      </Widget>
    );
  }
}


const BiodataForm = Form.create()(Biodata);

const mapStateToProps = ({generic}) => {
  const { requests } = generic;
  return { requests }
};

export default connect(mapStateToProps, {
  setRequestState
})(BiodataForm);
