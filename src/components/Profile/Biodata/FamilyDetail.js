import React, { Component } from "react";
import {Button, Col, Form, Input, DatePicker} from "antd";
import { IntlMessages, ValidationMessages } from "util/index";
import PropTypes from "prop-types";
import SelectSearch from "../../SelectSearch";

const FormItem = Form.Item;

class FamilyDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false
    }
  }

  componentDidMount() {
    if (this.props.isEditState) {
      this.setState({isEdit: true})
    }
  }

  changeComponentState = (state) => {
    this.setState({isEdit: state})
  };

  render() {
    const { getFieldDecorator, formItemLayout, isEditState, relationState } = this.props;
    const { isEdit } = this.state;

    const educationList = [
      {id: '1', name: 'SD'},
      {id: '2', name: 'SLTP'},
      {id: '3', name: 'SLTA'},
      {id: '4', name: 'D3'},
      {id: '5', name: 'S1'},
      {id: '6', name: 'S2'},
      {id: '7', name: 'S3'},
    ];

    let relationList;
    switch (relationState) {
      case 'suamiIstri':
        relationList = [
          {id: '1', name: 'Suami'},
          {id: '2', name: 'Istri'}
        ];
        break;
      case 'anak':
        relationList = [
          {id: '1', name: 'Anak Kandung'},
          {id: '2', name: 'Anak Angkat'}
        ];
        break;
      case 'orangTua':
        relationList = [
          {id: '1', name: 'Bapak Kandung'},
          {id: '2', name: 'Bapak Tiri'},
          {id: '3', name: 'Ibu Kandung'},
          {id: '4', name: 'Ibu Tiri'},
        ];
        break;
      default:
        relationList = [];
        break;
    }

    const SuamiIstriEdit =
      <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label={<IntlMessages id='profile.nik'/>}>
          {getFieldDecorator('nik', {
            rules: [{
              required: true, whitespace: true,
              message: <ValidationMessages fieldName='profile.nik' type='label.isRequired'/>
            }]
          })(<Input/>)}
        </FormItem>

        <FormItem {...formItemLayout} label={<IntlMessages id='profile.name'/>}>
          {getFieldDecorator('name', {
            rules: [{
              required: true, whitespace: true,
              message: <ValidationMessages fieldName='profile.name' type='label.isRequired'/>
            }]
          })(<Input/>)}
        </FormItem>

        <FormItem {...formItemLayout} label={<IntlMessages id='profile.placeBornDate'/>}>
          <div className="ant-row gx-form-row0">
            <Col xs={24} sm={12}>
              <FormItem>
                {getFieldDecorator('bornCity', {
                  rules: [{ required: true, whitespace: true, message: <ValidationMessages fieldName='profile.bornCity' type='label.isRequired'/> }]
                })(<Input/>)}
              </FormItem>
            </Col>
            <Col xs={24} sm={12}>
              <FormItem>
                <FormItem>
                  {getFieldDecorator('bornDate', {
                    rules: [{ required: true, whitespace: true, message: <ValidationMessages fieldName='profile.bornDate' type='label.isRequired'/> }]
                  })(<DatePicker/>)}
                </FormItem>
              </FormItem>
            </Col>
          </div>
        </FormItem>

        <FormItem {...formItemLayout} label={<IntlMessages id='profile.education'/>}>
          <SelectSearch data={educationList} getFieldDecorator={getFieldDecorator} name='education'
                        label='profile.education' placeholder={<IntlMessages id='profile.education'/>}/>
        </FormItem>

        <FormItem {...formItemLayout} label={<IntlMessages id='profile.job'/>}>
          {getFieldDecorator('job', {
            rules: [{ required: true, whitespace: true, message: <ValidationMessages fieldName='profile.job' type='label.isRequired'/> }]
          })(<Input/>)}
        </FormItem>

        <FormItem {...formItemLayout} label={<IntlMessages id='profile.relation'/>}>
          <SelectSearch data={relationList} getFieldDecorator={getFieldDecorator} name='relation'
                        label='profile.relation' placeholder={<IntlMessages id='profile.relation'/>}/>
        </FormItem>

        {
          isEditState ? null :
            <FormItem
              wrapperCol={{xs: 24, sm: {span: 24, offset: 6}}} className="gx-text-right gx-mb-0">
              <Button type="primary" htmlType="button" onClick={() => this.changeComponentState(false)}><IntlMessages id='button.save'/></Button>
            </FormItem>
        }
      </Form>;

    const SuamiIstriView =
      <Form>
        <FormItem style={{ marginBottom: 0 }} {...formItemLayout} label={<IntlMessages id='profile.nik'/>}>
          <p>3173052197616667</p>
        </FormItem>

        <FormItem style={{ marginBottom: 0 }} {...formItemLayout} label={<IntlMessages id='profile.name'/>}>
          <p>Jokowi Ahmad</p>
        </FormItem>

        <FormItem style={{ marginBottom: 0 }} {...formItemLayout} label={<IntlMessages id='profile.placeBornDate'/>}>
          <p>Manado, 11 Maret 1956</p>
        </FormItem>

        <FormItem style={{ marginBottom: 0 }} {...formItemLayout} label={<IntlMessages id='profile.education'/>}>
          <p>S1</p>
        </FormItem>

        <FormItem style={{ marginBottom: 0 }} {...formItemLayout} label={<IntlMessages id='profile.job'/>}>
          <p>Pegawai Negeri Sipil</p>
        </FormItem>

        <FormItem style={{ marginBottom: 0 }} {...formItemLayout} label={<IntlMessages id='profile.relation'/>}>
          <p>Suami</p>
        </FormItem>

        <FormItem style={{ marginBottom: 0 }}
          wrapperCol={{xs: 24, sm: {span: 24, offset: 6}}} className="gx-text-right gx-mb-0">
          <Button type="primary" htmlType="button" onClick={() => this.changeComponentState(true)}><IntlMessages id='button.edit'/></Button>
        </FormItem>
      </Form>;

    return isEdit ? SuamiIstriEdit : SuamiIstriView;
  }
}

FamilyDetail.propTypes = {
  getFieldDecorator: PropTypes.any.isRequired,
  formItemLayout: PropTypes.object,
  isEditState: PropTypes.bool,
  relationState: PropTypes.string
};

export default FamilyDetail;
