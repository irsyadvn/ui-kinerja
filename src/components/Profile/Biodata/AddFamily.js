import React, { Component } from "react";
import { IntlMessages, ValidationMessages } from "util/index";
import {Form, Row, Col, Select, Input, Table, Tooltip} from "antd";
import PropTypes from 'prop-types';

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const data = [
  {
    key: '1',
    id: 1,
    itemOfPositionActivities: 'Membackup Data',
    unitOfResults: 'Data',
    creditNumber: '0',
  },
  {
    key: '2',
    id: 2,
    itemOfPositionActivities: 'Membuat Konsep Proses Bisnis',
    unitOfResults: 'Proses Draft',
    creditNumber: '0',
  },
  {
    key: '3',
    id: 3,
    itemOfPositionActivities: 'Membuat Laporan Aplikasi',
    unitOfResults: 'Laporan',
    creditNumber: '0',
  },
  {
    key: '4',
    id: 4,
    itemOfPositionActivities: 'Mengembangkan Aplikasi',
    unitOfResults: 'Aplikasi',
    creditNumber: '0',
  },
];

const mainPerformanceIndicatorData = [
  {
    key: '1',
    id: 1,
    mainPerformanceIndicator: 'IKU Saja 1',
    target: '1 Dokumen'
  },
  {
    key: '2',
    id: 2,
    mainPerformanceIndicator: 'IKU Saja 2',
    target: '1 Dokumen'
  },
  {
    key: '3',
    id: 3,
    mainPerformanceIndicator: 'IKU Saja 3',
    target: '1 Dokumen'
  }
];

const strategicPerformanceIndicatorData = [
  {
    key: '1',
    id: 1,
    strategicPerformanceIndicator: 'IKU Saja 1',
    target: '1 Dokumen'
  },
  {
    key: '2',
    id: 2,
    strategicPerformanceIndicator: 'IKU Saja 2',
    target: '1 Dokumen'
  },
  {
    key: '3',
    id: 3,
    strategicPerformanceIndicator: 'IKU Saja 3',
    target: '1 Dokumen'
  }
];

class AddFamily extends Component {
  constructor(state) {
    super(state);
    this.state = {
    }
  }

  copyTable = () => {
    console.log('Data copied!');
  };

  render() {
    const { getFieldDecorator } = this.props;
    const formItemLayout = {
      labelCol: {xs: 24, sm: 8},
      wrapperCol: {xs: 24, sm: 16},
    };

    const supervisorAnnualSkpList = [
      {id: '001', name: 'Belum Cascading'}
    ];

    const strategicPlanList = [
      {id: '1', name: 'Membuat Kinerja 3'}
    ];

    const columns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        width: 30,
      },
      {
        title: <IntlMessages id="skp.itemOfPositionActivities"/>,
        dataIndex: 'itemOfPositionActivities',
        key: 'itemOfPositionActivities'
      },
      {
        title: <IntlMessages id="skp.unitOfResults"/>,
        dataIndex: 'unitOfResults',
        key: 'unitOfResults'
      },
      {
        title: <IntlMessages id="skp.creditNumber"/>,
        dataIndex: 'creditNumber',
        key: 'creditNumber'
      },
      {
        title: <IntlMessages id="button.copy"/>,
        dataIndex: '',
        key: 'copy',
        render: () => <Tooltip placement="right" title={<IntlMessages id="button.copy"/>}><span className="gx-link" onClick={() => this.copyTable}><i className="icon fa fa-copy"/></span></Tooltip>,
      }
    ];

    const mainColumns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        width: 30,
      },
      {
        title: <IntlMessages id="skp.mainPerformanceIndicator"/>,
        dataIndex: 'mainPerformanceIndicator',
        key: 'mainPerformanceIndicator'
      },
      {
        title: <IntlMessages id="skp.target"/>,
        dataIndex: 'target',
        key: 'target'
      }
    ];

    const strategicColumns = [
      {
        title: 'No',
        dataIndex: 'id',
        key: 'id',
        width: 30,
      },
      {
        title: <IntlMessages id="skp.strategicPerformanceIndicator"/>,
        dataIndex: 'strategicPerformanceIndicator',
        key: 'strategicPerformanceIndicator'
      },
      {
        title: <IntlMessages id="skp.target"/>,
        dataIndex: 'target',
        key: 'target'
      }
    ];

    const quantityUnit = getFieldDecorator('quantityUnit', {
      initialValue: 'dokumen',
    })(
      <Select style={{width: 130}}>
        <Option value="dokumen">Dokumen</Option>
        <Option value="surat">Surat</Option>
        <Option value="prosesDraft">Proses Draft</Option>
      </Select>
    );

    return (
      <div className='add-target-component'>
        <Row>
          <Col lg={10} md={10} sm={24} xs={24}>
            <Form>
              <FormItem
                {...formItemLayout}
                label={<IntlMessages id='skp.period'/>} className='gx-mb-0'
              >
                <span className="ant-form-text">2 Jan - 30 Jun 2017</span>
              </FormItem>
              <FormItem
                {...formItemLayout}
                label={<IntlMessages id='skp.supervisorAnnualSkp'/>}>
                {getFieldDecorator('supervisorAnnualSkp', {
                  rules: [
                    {required: true, message: <ValidationMessages name='skp.supervisorAnnualSkp' type='label.isRequired'/>},
                  ],
                  initialValue: '001',
                })(
                  <Select placeholder={<IntlMessages id='skp.supervisorAnnualSkp'/>}>
                    {
                      supervisorAnnualSkpList.map((item, i) => (
                        <Option key={i} value={item.id}>{item.name}</Option>
                      ))
                    }
                  </Select>
                )}
              </FormItem>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.annualActivity'/>}>
                {getFieldDecorator('annualActivity', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.annualActivity' type='label.isRequired'/>
                  }]
                })(<TextArea rows={4}/>)}
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.quantityTarget'/>}>
                {getFieldDecorator('quantityTarget', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.quantityTarget' type='label.isRequired'/>
                  }]
                })(<Input addonAfter={quantityUnit} style={{width: '100%'}}/>)}
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.timeTarget'/>}>
                {getFieldDecorator('timeTarget', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.timeTarget' type='label.isRequired'/>
                  }]
                })(<Input/>)}
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label={<IntlMessages id='skp.cost'/>}>
                {getFieldDecorator('cost', {
                  rules: [{
                    required: true, whitespace: true,
                    message: <ValidationMessages fieldName='skp.cost' type='label.isRequired'/>
                  }]
                })(<Input/>)}
              </Form.Item>
            </Form>
          </Col>
          <Col lg={14} md={14} sm={24} xs={24}>
            <p className='gx-text-center'><b>JABATAN PRANATA KOMPUTER PERTAMA</b></p>
            {/*<Card>*/}
              <div className="table-operations gx-d-flex ant-row-flex-space-between">
                {/*<Button><i className='fa fa-print gx-mr-1'/><IntlMessages id="button.print"/></Button>*/}
              </div>
              <Table className="gx-table-responsive center" columns={columns} dataSource={data} bordered={true} onChange={this.handleChange} pagination={{pageSize: 5}}/>
            {/*</Card>*/}
          </Col>
          <Col lg={24} md={24} sm={24} xs={24}>
            <div className='gx-text-center'>
              <p className='gx-mb-0'><b>RENCANA KERJA TAHUNAN</b></p>
              <p><b>DIREKTORAT KINERJA APARATUR SIPIL NEGARA 2019</b></p>
              <Col lg={10} md={10} sm={24} xs={24}>
                <Form>
                  <FormItem
                    {...formItemLayout}
                    label={<IntlMessages id='skp.strategicPlan'/>}>
                    {getFieldDecorator('strategicPlan', {
                      initialValue: '1',
                    })(
                      <Select placeholder={<IntlMessages id='skp.strategicPlan'/>}>
                        {
                          strategicPlanList.map((item, i) => (
                            <Option key={i} value={item.id}>{item.name}</Option>
                          ))
                        }
                      </Select>
                    )}
                  </FormItem>
                </Form>
              </Col>
            </div>

          </Col>
          <Col lg={12} md={12} sm={24} xs={24}>
            <Table className="gx-table-responsive center" columns={mainColumns} dataSource={mainPerformanceIndicatorData}
                   bordered={true} onChange={this.handleChange} pagination={false} size='middle'/>
          </Col>
          <Col lg={12} md={12} sm={24} xs={24}>
            <Table className="gx-table-responsive center" columns={strategicColumns} dataSource={strategicPerformanceIndicatorData}
                   bordered={true} onChange={this.handleChange} pagination={false} size='middle'/>
          </Col>
        </Row>
      </div>
    );
  }
}

AddFamily.propTypes = {
  getFieldDecorator: PropTypes.any
};

export default AddFamily;
