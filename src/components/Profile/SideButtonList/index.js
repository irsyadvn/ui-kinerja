import React from "react";
import {Badge} from "antd";
import Widget from "../../Widget/index";


function Status(props) {
  const isType = props.isType;
  if (isType === 'online') {
    return <Badge status="success"/>;
  } else if (isType === 'away') {
    return <Badge status="warning"/>;
  } else {
    return <Badge count={0} status="error"/>;
  }
}

const SideButtonList = ({buttonList}) => {
  return (
    <Widget styleName="gx-card-profile-sm"
            title={<span>Schedule</span>}>
      <div className="gx-pt-2">
        <ul className="gx-list-inline">
          {buttonList.map((button) =>
            <li key={button.id}>
              <span className="gx-link gx-btn gx-btn-primary gx-mb-10">{button.name}</span>
            </li>
          )}
        </ul>
      </div>
    </Widget>
  )
};
export default SideButtonList;
