import React, { Component } from "react";
import {Select} from "antd";
import PropTypes from "prop-types";
import { ValidationMessages } from "util/index";

const Option = Select.Option;

class SelectSearch extends Component {
  render() {
    const { data, placeholder, getFieldDecorator, label, name, required } = this.props;
    function handleChange(value) {
      console.log(`selected ${value}`);
    }

    function handleBlur() {
      console.log('blur');
    }

    function handleFocus() {
      console.log('focus');
    }

    return (
      <div>
        {getFieldDecorator(name, {
          rules: [{
            required: required, whitespace: true,
            message: <ValidationMessages name={label} type='label.isRequired'/>
          }]
        })(
          <Select
            showSearch
            placeholder={placeholder}
            optionFilterProp="children"
            onChange={handleChange}
            onFocus={handleFocus}
            onBlur={handleBlur}
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
              option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {
              data.map((item, i) => {
                return (
                  <Option value={item.id} key={i}>{item.name}</Option>
                )
              })
            }
          </Select>
        )}
      </div>
    );
  }
}

ValidationMessages.defaultProps = {
  required: false,
};

SelectSearch.propTypes = {
  data: PropTypes.array.isRequired,
  getFieldDecorator: PropTypes.any.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.any,
  required: PropTypes.bool
};

export default SelectSearch;
