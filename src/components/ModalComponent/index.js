import React from "react";
import {Button, Modal} from "antd";
import PropTypes from 'prop-types';
import { IntlMessages } from "util/index";

const ModalComponent = (props) => {
  const childrenText = (type) => {
    switch(type) {
      case 'delete':
        return <IntlMessages id={'question.delete'}/>;
      case 'confirm':
        return <IntlMessages id={'question.confirm'}/>;
      default:
        return <IntlMessages id={'question.confirm'}/>
    }
  };
  return (
    <Modal
      {...props}
      onOk={props.onSubmit}
      width={props.size === 'large' ? '90%' : '520px'}
      footer={[
        <Button key="back" onClick={props.onCancel}>
          {props.cancelTextButton ? props.cancelTextButton : <IntlMessages id='button.cancel'/>}
        </Button>,
        {...props.onSubmit ?
            <Button key="submit" type="primary" loading={props.loading} onClick={props.onSubmit}>
              {props.submitTextButton ? props.submitTextButton : <IntlMessages id='button.submit'/>}
            </Button> : null
        },
      ]}
    >
      {
        props.children ? props.children : <p>{childrenText(props.type)}</p>
      }
    </Modal>
  )
};

ModalComponent.propTypes = {
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  loading: PropTypes.bool,
  title: PropTypes.any,
  cancelTextButton: PropTypes.string,
  submitTextButton: PropTypes.any,
  type: PropTypes.string,
  children: PropTypes.any,
  size: PropTypes.string,
  style: PropTypes.object
};

export default ModalComponent;
