import { SET_REQUEST_STATE } from "constants/ActionTypes";

export function setRequestState(requests) {
  return {
    type: SET_REQUEST_STATE,
    requests: requests
  }
}
