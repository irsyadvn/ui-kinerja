import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import Settings from "./Settings";
import Auth from "./Auth";
import Generic from './GenericReducers';


const reducers = combineReducers({
  routing: routerReducer,
  settings: Settings,
  auth: Auth,
  generic: Generic
});

export default reducers;
