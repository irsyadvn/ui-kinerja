import {
  SET_REQUEST_STATE
} from "constants/ActionTypes";
import {fromJS} from 'immutable';
import requests from '../states';

const initialState = {
  ...requests
};

const generic = (state = initialState, action) => {
  switch(action.type){
    case SET_REQUEST_STATE:
      return {
        ...state,
        requests: fromJS(state.requests).mergeDeep(action.requests).toJS()
      };
    default:
      return state;
  }
};

export default generic;
