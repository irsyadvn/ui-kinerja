import { takeEvery, put } from 'redux-saga/effects';
import {
  SET_REQUEST_STATE
} from "constants/ActionTypes";
import { setRequestState } from "../actions";


function* setRequest(response){
  console.log('Saga Request State!');
  yield put(setRequestState(response));
}

export const genericSagas = [
    takeEvery(SET_REQUEST_STATE, setRequest),
];
