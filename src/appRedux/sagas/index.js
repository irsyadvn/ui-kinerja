import {all} from "redux-saga/effects";
import authSagas from "./Auth";
import {genericSagas} from "./Generic";

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    ...genericSagas
  ]);
}
