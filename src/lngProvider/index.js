import enLang from "./entries/en-US";
import idLang from "./entries/id_ID";
import {addLocaleData} from "react-intl";

const AppLocale = {
  en: enLang,
  id: idLang,
};
addLocaleData(AppLocale.en.data);
addLocaleData(AppLocale.id.data);

export default AppLocale;
