import antdId from "antd/lib/locale-provider/id_ID";
import appLocaleData from "react-intl/locale-data/id";
import idMessages from "../locales/id_ID.json";

const IdLang = {
  messages: {
    ...idMessages
  },
  antd: antdId,
  locale: 'id',
  data: appLocaleData
};
export default IdLang;
