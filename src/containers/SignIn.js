import React from "react";
import {Button, Checkbox, Form, Input, message, Tag} from "antd";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

import {
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGithubSignIn,
  userGoogleSignIn,
  userSignIn,
  userTwitterSignIn
} from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignIn extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.showAuthLoader();
        console.log('val: ', values);
        this.props.userSignIn(values);
      }
    });
  };

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const {showMessage, loader, alertMessage} = this.props;

    return (
      <div className="gx-app-login-outer-wrap">
        <h1 className="gx-text-center login-title">E-Kinerja Kabupaten Minahasa Selatan</h1>
        <div className="gx-app-login-wrap">
          <div className="best-employee">
            <h3>PEGAWAI TERBAIK BULAN MARET TAHUN 2019</h3>
            <Tag>78.67</Tag>
            <p>HASANUDIN, S.E</p>
            <p>196103211983121001</p>
            <p className="gx-mb-5">Kepala Seksi Analisis Sistem Informasi Kinerja Pegawai Aparatur Sipil Negara</p>
            <small>Note: Penilaian dilakukan oleh Tim Kinerja BKN Pusat dengan mempertimbangkan berbagai aspek</small>
          </div>
          <div className="gx-app-login-container">
            <div className="gx-app-login-main-content">
              {/*<div className="gx-app-logo-content">*/}
              {/*<div className="gx-app-logo-content-bg">*/}
              {/*<img src={require("assets/images/background/kantor-bupati-minsel.jpg")} alt='Neature'/>*/}
              {/*</div>*/}
              {/*<div className="gx-app-logo-wid">*/}
              {/*<h1><IntlMessages id="app.userAuth.signIn"/></h1>*/}
              {/*/!*<p><IntlMessages id="app.userAuth.bySigning"/></p>*!/*/}
              {/*</div>*/}
              {/*<div className="gx-app-logo">*/}
              {/*/!*<img alt="example" src={require("assets/images/logo.png")}/>*!/*/}
              {/*</div>*/}
              {/*</div>*/}
              <div className="gx-app-login-content">
                <Form onSubmit={this.handleSubmit} className="gx-signin-form gx-form-row0">

                  <FormItem>
                    {getFieldDecorator('email', {
                      initialValue: "jejakminsel@gmail.com",
                      rules: [{
                        required: true, type: 'email', message: 'Inputan bukan surel yang valid!',
                      }],
                    })(
                      <Input placeholder="Surel"/>
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('password', {
                      initialValue: "password",
                      rules: [{required: true, message: 'Silakan masukan kata sandi Anda!'}],
                    })(
                      <Input type="password" placeholder="Kata Sandi"/>
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('remember', {
                      valuePropName: 'checked',
                      initialValue: true,
                    })(
                      <Checkbox><IntlMessages id="appModule.rememberMe"/></Checkbox>
                    )}
                  </FormItem>
                  <FormItem className="gx-text-center">
                    <Button type="primary" className="gx-mb-0 gx-w-100" htmlType="submit">
                      <IntlMessages id="app.userAuth.signIn"/>
                    </Button>
                    <Link to="/"><IntlMessages
                    id="app.userAuth.forgotPassword"/>?</Link>
                  </FormItem>
                </Form>
              </div>

              {loader ?
                <div className="gx-loader-view">
                  <CircularProgress/>
                </div> : null}
              {showMessage ?
                message.error(alertMessage.toString()) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(SignIn);

const mapStateToProps = ({auth}) => {
  console.log('auth: ', auth);
  const {loader, alertMessage, showMessage, authUser} = auth;
  return {loader, alertMessage, showMessage, authUser}
};

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGoogleSignIn,
  userGithubSignIn,
  userTwitterSignIn
})(WrappedNormalLoginForm);
