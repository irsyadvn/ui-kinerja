import React, {Component} from "react";
import {Avatar, Popover} from "antd";
import {connect} from "react-redux";
import {userSignOut} from "appRedux/actions/Auth";
import { IntlMessages } from "util/index";
import {Link} from "react-router-dom";

class UserProfile extends Component {

  render() {
    const userMenuOptions = (
      <ul className="gx-user-popover">
        <li><Link to='/profile'><span className='text-black'><IntlMessages id="sidebar.myAccount"/></span></Link></li>
        <li onClick={() => this.props.userSignOut()}><IntlMessages id="sidebar.logout"/></li>
      </ul>
    );

    return (

      <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row">
        <Popover placement="bottomRight" content={userMenuOptions} trigger="click">
          <Avatar src='https://via.placeholder.com/150x150'
                  className="gx-size-40 gx-pointer gx-mr-3" alt=""/>
          <span className="gx-avatar-name">E-Kinerja Minsel<i
            className="icon icon-chevron-down gx-fs-xxs gx-ml-2"/></span>
        </Popover>
      </div>

    )

  }
}

export default connect(null, {userSignOut})(UserProfile);
