import React, {Component} from "react";
import {connect} from "react-redux";
import {Menu} from "antd";
import {Link} from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";

import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import {userSignOut} from "appRedux/actions/Auth";

const SubMenu = Menu.SubMenu;


class SidebarContent extends Component {

  getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  render() {
    const {themeType, navStyle, pathname} = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (<Auxiliary>

        <SidebarLogo/>
        <div className="gx-sidebar-content">
          <div className={`gx-sidebar-notifications ${this.getNoHeaderClass(navStyle)}`}>
            <UserProfile/>
          </div>
          <CustomScrollbars className="gx-layout-sider-scrollbar">
            <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
              mode="inline">

              <Menu.Item key="home">
                <Link to="/home"><i className="icon fa fa-home"/><IntlMessages id="sidebar.home"/></Link>
              </Menu.Item>

              <Menu.Item key="profile">
                <Link to="/profile"><i className="icon fa fa-user"/><IntlMessages id="sidebar.profile"/></Link>
              </Menu.Item>

              <Menu.Item key="data-pegawai">
                <Link to="/data-pegawai"><i className="icon fa fa-users"/><IntlMessages id="sidebar.employeeData"/></Link>
              </Menu.Item>

              <SubMenu key="skp" className={this.getNavStyleSubMenuClass(navStyle)} title={
                <span>
                  <i className="icon icon-widgets"/>
                  <IntlMessages id="sidebar.skp"/>
              </span>}>
                <Menu.Item key="skp-tahunan">
                  <Link to="/skp-tahunan"><IntlMessages id="sidebar.annualSkp"/></Link>
                </Menu.Item>
                <Menu.Item key="skp-bulanan">
                  <Link to="/skp-bulanan"><IntlMessages id="sidebar.monthlySkp"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/2">
                  <Link to="/home"><IntlMessages id="sidebar.subordinateAnnualSkp"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/3">
                  <Link to="/home"><IntlMessages id="sidebar.subordinateMonthlySkp"/></Link>
                </Menu.Item>
              </SubMenu>

              <SubMenu key="monthlyBehavior" className={this.getNavStyleSubMenuClass(navStyle)} title={
                <span>
                  <i className="icon fa fa-user-tie"/>
                  <IntlMessages id="sidebar.monthlyBehavior"/>
              </span>}>
                <Menu.Item key="components/navigation/affix">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu1"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/breadcrumb">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu2"/></Link>
                </Menu.Item>
              </SubMenu>

              <SubMenu key="productivity" className={this.getNavStyleSubMenuClass(navStyle)} title={
                <span>
                  <i className="icon fa fa-cogs"/>
                  <IntlMessages id="sidebar.productivity"/>
              </span>}>
                <Menu.Item key="components/navigation/affix">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu1"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/breadcrumb">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu2"/></Link>
                </Menu.Item>
              </SubMenu>

              <Menu.Item key="dailyReportAdditionalTask">
                <Link to="/home"><i className="icon fa fa-tasks"/><IntlMessages id="sidebar.dailyReportAdditionalTask"/></Link>
              </Menu.Item>

              <SubMenu key="disposition" className={this.getNavStyleSubMenuClass(navStyle)} title={
                <span>
                  <i className="icon fa fa-sticky-note"/>
                  <IntlMessages id="sidebar.disposition"/>
              </span>}>
                <Menu.Item key="components/navigation/affix">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu1"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/breadcrumb">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu2"/></Link>
                </Menu.Item>
              </SubMenu>

              <Menu.Item key="kreatifitas">
                <Link to="/kreatifitas"><i className="icon fa fa-lightbulb"/><IntlMessages id="sidebar.creativity"/></Link>
              </Menu.Item>

              <SubMenu key="annualSkpDetail" className={this.getNavStyleSubMenuClass(navStyle)} title={
                <span>
                  <i className="icon fa fa-info-circle"/>
                  <IntlMessages id="sidebar.annualSkpDetail"/>
              </span>}>
                <Menu.Item key="components/navigation/affix">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu1"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/breadcrumb">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu2"/></Link>
                </Menu.Item>
              </SubMenu>

              <Menu.Item key="editSubordinateStaff">
                <Link to="/home"><i className="icon fa fa-edit"/><IntlMessages id="sidebar.editSubordinateStaff"/></Link>
              </Menu.Item>

              <SubMenu key="dailyReportSkp" className={this.getNavStyleSubMenuClass(navStyle)} title={
                <span>
                  <i className="icon fa fa-tachometer-alt"/>
                  <IntlMessages id="sidebar.dailyReportSkp"/>
              </span>}>
                <Menu.Item key="components/navigation/affix">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu1"/></Link>
                </Menu.Item>
                <Menu.Item key="components/navigation/breadcrumb">
                  <Link to="/home">
                    <IntlMessages
                      id="sidebar.subMenu2"/></Link>
                </Menu.Item>
              </SubMenu>

              <Menu.Item key="logout">
                <div onClick={() => this.props.userSignOut()}><i className="icon fa fa-sign-out-alt"/><IntlMessages id="sidebar.logout"/></div>
              </Menu.Item>

            </Menu>
          </CustomScrollbars>
        </div>
      </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({settings}) => {
  const {navStyle, themeType, locale, pathname} = settings;
  return {navStyle, themeType, locale, pathname}
};
export default connect(mapStateToProps, {userSignOut})(SidebarContent);

