import React from "react";
import PropTypes from "prop-types";
import IntlMessage from "./IntlMessages";

const ValidationMessages = ({fieldName, type}) => {
  return (
    <span>
      <IntlMessage id={fieldName}/> <IntlMessage id={type}/>
    </span>
  )
};

ValidationMessages.defaultProps = {
  type: 'isRequired',
};

ValidationMessages.propTypes = {
  fieldName: PropTypes.node,
  type: PropTypes.node,
};

export default ValidationMessages;
