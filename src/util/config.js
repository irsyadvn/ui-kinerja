const year = new Date().getFullYear();
module.exports = {
  footerText: 'Copyright Kasta Group Indonesia \u00A9 ' + year,
};
