import React from "react";
import PropTypes from "prop-types";

const DateFormatter = ({date, format}) => {
  const [year, month, day] = date.split("-");
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
  let formattedDate;
  switch (format) {
    case 'dd mon yyyy':
      formattedDate = day + ' ' + monthNames[month-1] + ' ' + year;
      break;
    default:
      formattedDate = day + ' ' + monthNames[month-1] + ' ' + year;
      break;
  }
  return (
    <span>{formattedDate}</span>
  )
};

DateFormatter.defaultProps = {
};


DateFormatter.propTypes = {
  date: PropTypes.string,
  format: PropTypes.node,
};

export default DateFormatter;
